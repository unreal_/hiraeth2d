#include "heUtils.h"
#include <algorithm>

bool HeStringUtils::startsWith(const std::string& base, const std::string& check) {
	return base.compare(0, check.size(), check) == 0;
}

bool HeStringUtils::endsWith(const std::string& base, const std::string& check) {
	if (base.size() < check.size())
		return false;
	return base.compare(base.size() - check.size(), base.size(), check) == 0;
}

void HeStringUtils::ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
                                    return !isspace(ch);
                                    }));
}

void HeStringUtils::rtrim(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
                         return !isspace(ch);
                         }).base(), s.end());
}

void HeStringUtils::trim(std::string& s) {
	ltrim(s);
	rtrim(s);
}

void HeStringUtils::split(const std::string& s, const char& delim, std::vector<std::string>& result) {
	result.clear();
    
	std::string::const_iterator start = s.begin();
	std::string::const_iterator end = s.end();
	std::string::const_iterator next = std::find(start, end, delim);
	while (next != end) {
		result.emplace_back(std::string(start, next));
		start = next + 1;
		next = std::find(start, end, delim);
	}
	result.emplace_back(std::string(start, next));
}

std::vector<std::string> HeStringUtils::split(const std::string& s, const char& delim) {
	std::vector<std::string> result;
	split(s, delim, result);
	return result;
}

bool HeStringUtils::equals(const std::string& s1, const std::string& s2) {
	return s1.compare(s2) == 0;
}

bool HeStringUtils::isAlphaNumeric(std::string& str) {
	return std::find_if(str.begin(), str.end(), [](char c) { return !(isalnum(c) || (c == ' ')); }) == str.end();
}

std::string HeStringUtils::replaceAll(const std::string& input, const std::string& from, const std::string& to) {
	if (from.empty())
		return input;
	
	std::string out = input;
	size_t start_pos = 0;
	while ((start_pos = out.find(from, start_pos)) != std::string::npos) {
		out.replace(start_pos, from.length(), to);
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
    
	return out;
}

unsigned int HeStringUtils::count(const std::string& input, const char character) {
	unsigned int count = 0;
	for (unsigned int i = 0; i < (unsigned int)input.size(); ++i)
		if (input[i] == character)
        count++;
    
	return count;
}
