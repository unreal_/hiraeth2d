#include "jsonreader.h"
#include <fstream>
#include <iostream>
#include <cctype>
#include <string>

HeJsonToken::HeJsonToken(const HeJsonToken& token) {
    
    this->type = token.type;
    this->lineIndex = token.lineIndex;
    
	switch(this->type) {
        case HE_JSON_TOKEN_TYPE_STRING:
        this->_string = token._string; 
        break;
        
        case HE_JSON_TOKEN_TYPE_NUMBER:
        this->_number = token._number;
        break;
        
        default:
        this->_token = token._token;
        break;
    }
    
}


const HeJsonEntry& HeJsonEntry::operator[](const unsigned int index) const {
    if(index >= array.size()) {
        std::cout << "Warning: Tried to access json array out of bounds: " << key << " : " << index << "/" << array.size() << std::endl;
        return array[array.size() - 1];
    }
    
    return array[index];
}

const HeJsonEntry& HeJsonEntry::operator[](const std::string& index) const {
    auto it = object.find(index);
    if(it == object.end()) {
        std::cout << "Warning: No such child in json object: " << index << " in " << key << std::endl;
        return object.begin()->second;
    }
    
    return it->second;
}



std::string heJsonToString(const HeJsonTokenType& type) {
    
    std::string name;
    switch(type) {
        
        case HE_JSON_TOKEN_TYPE_CURLY_BRACKET_LEFT:
        name = "{";
        break;
        
        case HE_JSON_TOKEN_TYPE_CURLY_BRACKET_RIGHT:
        name = "}";
        break;
        
        case HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_LEFT:
        name = "[";
        break;
        
        case HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_RIGHT:
        name = "]";
        break;
        
        case HE_JSON_TOKEN_TYPE_QUOTE:
        name = "\"";
        break;
        
        case HE_JSON_TOKEN_TYPE_COMMA:
        name = ",";
        break;
        
        case HE_JSON_TOKEN_TYPE_COLON:
        name = ";";
        break;
        
        case HE_JSON_TOKEN_TYPE_SEMI_COLON:
        name = ":";
        break;
        
        case HE_JSON_TOKEN_TYPE_STRING:
        name = "_string_";
        break;
        
        case HE_JSON_TOKEN_TYPE_NUMBER:
        name = "_number_";
        break;
        
        default:
        name = "UnknownToken";
        break;
    }
    
    return name;
    
}

std::string heJsonToString(const HeJsonToken& token) {
    
    std::string str;
    switch(token.type) {
        case HE_JSON_TOKEN_TYPE_STRING:
        str = token._string;
        break;
        
        case HE_JSON_TOKEN_TYPE_NUMBER:
        str = std::to_string(token._number);
        break;
        
        default:
        str = std::string(1, token._token);
    }
    
    return str;
    
}


HeJsonToken& heJsonGetNextToken(HeJsonTokenVector* vector) {
    
    if(vector->index == vector->tokens.size() - 1) {
        // we reached the end of the vector
        std::cout << "Error: Reached end of json vector (" << vector->index << "/" << vector->tokens.size() << ")" << std::endl;
        return vector->emptyToken;
    }
    
    return vector->tokens[++vector->index];
    
}

HeJsonToken& heJsonPeekNextToken(HeJsonTokenVector* vector) {
    
    if(vector->index == vector->tokens.size() - 1) {
        // we reached the end of the vector
        std::cout << "Error: Reached end of json vector (" << vector->index << "/" << vector->tokens.size() << ")" << std::endl;
        return vector->emptyToken;
    }
    
    return vector->tokens[vector->index + 1];
    
}

bool heJsonVectorAvailable(HeJsonTokenVector* vector) {
    
    return vector->index < vector->tokens.size() - 1;
    
}


void heJsonParseFile(HeJsonTokenVector* vector, const std::string& fileName) {
    
    std::ifstream stream(fileName);
    if(!stream) {
        std::cout << "Error: Could not find file [" << fileName << "] for reading!" << std::endl;
        return;
    }
    
    unsigned int lineIndex = 1;
    
	std::string line;
    while(getline(stream, line)) {
        
		//std::cout << "Char: " << currentChar << std::endl;
		for (unsigned int index = 0; index < (unsigned int) line.size(); ++index) {

			char currentChar = line[index];

			if (currentChar == ' ' || currentChar == '\t' || currentChar == '\n')
				continue;

			HeJsonTokenType type = HE_JSON_TOKEN_TYPE_NONE;

			switch (currentChar) {

			case '{':
				type = HE_JSON_TOKEN_TYPE_CURLY_BRACKET_LEFT;
				break;

			case '}':
				type = HE_JSON_TOKEN_TYPE_CURLY_BRACKET_RIGHT;
				break;

			case '[':
				type = HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_LEFT;
				break;

			case ']':
				type = HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_RIGHT;
				break;

			case '"':
				type = HE_JSON_TOKEN_TYPE_QUOTE;
				break;

			case ',':
				type = HE_JSON_TOKEN_TYPE_COMMA;
				break;

			case ':':
				type = HE_JSON_TOKEN_TYPE_COLON;
				break;

			case ';':
				type = HE_JSON_TOKEN_TYPE_SEMI_COLON;
				break;

			case 'f':
			case 't':
				// assume that this is true / false
				type = HE_JSON_TOKEN_TYPE_BOOLEAN;
				break;

			};

			if (type == HE_JSON_TOKEN_TYPE_NONE) {
				// assume that its a number, try to parse it
				if (std::isdigit(currentChar)) {
					std::string numberString = std::string(1, currentChar);
					while (index < (unsigned int) line.size()) {
						currentChar = line[++index];

						if (!std::isdigit(currentChar) && currentChar != '.')
							break;

						numberString += currentChar;
					}

					--index; // last char wasnt part of number, undo

					double number = std::stod(numberString);
					HeJsonToken token(HE_JSON_TOKEN_TYPE_NUMBER, lineIndex);
					token._number = number;
					vector->tokens.emplace_back(token);
					continue;
				} else {
					std::cout << "Error: Unknown character (" << currentChar << ") in json file [" << fileName << "] in line [" << lineIndex << "]" << std::endl;
					vector->tokens.clear();
					break;
				}
			}

			HeJsonToken token(type, lineIndex);
			token._token = currentChar;
			vector->tokens.emplace_back(token);
			
			if (type == HE_JSON_TOKEN_TYPE_BOOLEAN) {
				// skip until boolean is over
				while (std::isalpha(currentChar))
					currentChar = line[++index];
				continue;
			}

			if (type == HE_JSON_TOKEN_TYPE_QUOTE) {
				// parse string until next quote comes, then add read string as a token
				std::string string;
				while (index < (unsigned int) line.size()) {

					currentChar = line[++index];
					if (currentChar == '"')
						break;

					string += currentChar;
				}

				HeJsonToken stringToken(HE_JSON_TOKEN_TYPE_STRING, lineIndex);
				stringToken._string = string;
				vector->tokens.emplace_back(stringToken);

				HeJsonToken quoteToken(HE_JSON_TOKEN_TYPE_QUOTE, lineIndex);
				quoteToken._token = '"';
				vector->tokens.emplace_back(quoteToken);
			}


		}

		++lineIndex;

    }
    
    stream.close();
    
}


HeJsonEntry heJsonParseTokenVector(HeJsonTokenVector* vector) {
    
    HeJsonEntry root;
	root.key = "_root_";
    heJsonParse(vector, &root);
    return root;
    
}

HeJsonEntry heJsonLoadDocument(const std::string& fileName) {
    
    HeJsonTokenVector tokens;
    heJsonParseFile(&tokens, fileName);
	return heJsonParseTokenVector(&tokens);
    
}

bool heJsonObjectHasChild(const HeJsonEntry* entry, const std::string& childName) {
    
    return entry->object.find(childName) != entry->object.end();
    
}

void heJsonParse(HeJsonTokenVector* vector, HeJsonEntry* parent) {
    
    HeJsonToken& next = heJsonPeekNextToken(vector);
    //std::cout << "Parsing: " << heJsonToString(next) << std::endl;
    
    switch(next.type) {
        
        case HE_JSON_TOKEN_TYPE_CURLY_BRACKET_LEFT:
        heJsonParseObject(vector, parent);
        parent->type = HE_JSON_ENTRY_TYPE_OBJECT;
        break;
        
        case HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_LEFT:
        heJsonParseArray(vector, parent);
        parent->type = HE_JSON_ENTRY_TYPE_ARRAY;
        break;
        
        case HE_JSON_TOKEN_TYPE_QUOTE:
        heJsonGetNextToken(vector); // skip quote
        parent->string = heJsonGetNextToken(vector)._string;
        parent->type = HE_JSON_ENTRY_TYPE_STRING;
        heJsonGetNextToken(vector); // skip quote
        break;
        
        case HE_JSON_TOKEN_TYPE_NUMBER:
        parent->number = heJsonGetNextToken(vector)._number;
        parent->type = HE_JSON_ENTRY_TYPE_NUMBER;
        break;

		case HE_JSON_TOKEN_TYPE_BOOLEAN:
		parent->number = (next._token == 't');
		parent->type = HE_JSON_ENTRY_TYPE_BOOLEAN;
		break;
        
    }
    
}


void heJsonParseObject(HeJsonTokenVector* vector, HeJsonEntry* parent) {
    
    HeJsonToken& next = heJsonGetNextToken(vector);
    if(next.type != HE_JSON_TOKEN_TYPE_CURLY_BRACKET_LEFT) {
        std::cout << "Error: Expected object in line [" << next.lineIndex << "], got [" << heJsonToString(next) << "]" << std::endl;
        return;
    }
    
    while(heJsonVectorAvailable(vector)) {
        
        // parse name of entry
        heJsonGetNextToken(vector); // skip quote
        std::string name = heJsonGetNextToken(vector)._string;
		heJsonGetNextToken(vector); // skip quote
        heJsonGetNextToken(vector); // skip colon
        

		// WERE PARSING BOOL INSTEAD OF COMMA AFTER infite:false
		
        // parse entry value
        HeJsonEntry entry;
        entry.key = name;
        heJsonParse(vector, &entry);
        parent->object[name] = entry; // parent is an object, as were currently parsing it as object
        
        next = heJsonGetNextToken(vector);
        if(next.type == HE_JSON_TOKEN_TYPE_CURLY_BRACKET_RIGHT) {
            // reached end of this array
			break;
        }
    }

}

void heJsonParseArray(HeJsonTokenVector* vector, HeJsonEntry* parent) {
    
    HeJsonToken& next = heJsonGetNextToken(vector);
    if(next.type != HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_LEFT) {
        std::cout << "Error: Expected array in line [" << next.lineIndex << "], got [" << heJsonToString(next) << "]" << std::endl;
        return;
    }

	unsigned int childIndex = 0;
    
    while(heJsonVectorAvailable(vector)) {
        
        HeJsonEntry entry;
		entry.key = parent->key + "[" + std::to_string(childIndex) + "]";
        heJsonParse(vector, &entry);
        parent->array.emplace_back(entry); // parent is an array, as were currently parsing it as array
        
		++childIndex;

        next = heJsonGetNextToken(vector);
        if(next.type == HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_RIGHT) {
            // reached end of this array
            break;
        }
    }
    
}
