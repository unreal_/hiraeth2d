#include <iostream>
#include <fstream>
#include <filesystem>
#include "jsonreader.h"
#include "heUtils.h"

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "stb_image.h"
#include "stb_image_write.h"

struct Texture {
    unsigned int grid_start;   // grid index
    unsigned int tile_height;  // in pixels
    unsigned int tile_width;   // in pixels
    unsigned int tile_count;   // tile count
    unsigned int tile_columns; // in tiles
    unsigned int tile_rows;    // in tiles
	
	struct Frame {
		int width;
		int height;
		int channels;
		unsigned char* buffer;
	};

	// if this is a static image (no animation), frames[0] is the actual full image, else this vector has a 
	// frame for every animated image in the animation
	std::vector<Frame> frames;
	// the duration of one frame in the animation in ms
	int frame_duration = -1;
};

struct Level {
    std::vector<Texture> tilesets;
    int tile_width; // in pixels
    int tile_height; // in pixels
	int width; // in tiles
	int height; // in tiles
};

Level level;


// finds the texture that owns the given grid. A texture "owns" a grid if its grid_start is smaller or equal
// to grid_id and the tile_count of the texture plus its start is bigger than the grid_id
Texture* find_texture_for_grid(const unsigned int grid_id) {

	for (Texture& all : level.tilesets) {
		if (all.grid_start <= grid_id && all.grid_start + all.tile_count > grid_id)
			return &all;
	}

	std::cout << "Error: No tileset for grid " << grid_id << std::endl;
	return nullptr;

}

// takes the data of the background image (the tile information), gets that tile information (pixels) out of the
// according texture and stores the final background image
void parse_background_textures(const HeJsonEntry& data, const int width, const int height) {

	const unsigned int pixel_width = width * level.tile_width;
	const unsigned int pixel_height = height * level.tile_height;
	unsigned char* output = new unsigned char[pixel_width * pixel_height * 4];

	for (unsigned int tilex = 0; tilex < width; ++tilex) {
		for (unsigned int tiley = 0; tiley < height; ++tiley) {

			const unsigned int tile_index = tilex + tiley * width;
			const int global_grid_id = (int) data[tile_index];
			Texture* tex = find_texture_for_grid(global_grid_id);
			const int relative_grid_id = global_grid_id - tex->grid_start;
			const int texture_grid_x = (relative_grid_id % tex->tile_columns) * tex->tile_width;
			const int texture_grid_y = (relative_grid_id / tex->tile_columns) * tex->tile_height;

			for (unsigned int pixelx = 0; pixelx < level.tile_width; ++pixelx) {
				for (unsigned int pixely = 0; pixely < level.tile_height; ++pixely) {

					// output index
					const unsigned int totalx = tilex * level.tile_width + pixelx;
					const unsigned int totaly = tiley * level.tile_height + pixely;
					const unsigned int out_index = (totalx + totaly * pixel_width) * 4;

					// texture input
					const unsigned int tex_x = texture_grid_x + pixelx;
					const unsigned int tex_y = texture_grid_y + pixely;
					const unsigned int in_index = (tex_x + tex_y * tex->frames[0].width) * tex->frames[0].channels;

					output[out_index + 0] = tex->frames[0].buffer[in_index + 0];
					output[out_index + 1] = tex->frames[0].buffer[in_index + 1];
					output[out_index + 2] = tex->frames[0].buffer[in_index + 2];
					output[out_index + 3] = 255;

				}
			}

		}
	}

	stbi_write_png("out/background.png", pixel_width, pixel_height, 4, output, 0);
	std::cout << "Saved background" << std::endl;
	delete[] output;

}

// creates a png for the animation of t. This will put all frames next to each other (maximum column count is 8)
// and will write the resultng image. It will also append the animation data to the top of the file
void create_animation_file(Texture* t) {

	int columns = t->tile_count < 8 ? t->tile_count : 8;
	int rows = std::ceil(t->tile_count / columns);

	int width = 0, height = 0; // size of the output texture in pixels
	int xoffset = 0, yoffset = 0; // the offset of the current texture, in pixels

	for (int x = 0; x < columns; ++x) {
		for (int y = 0; y < rows; ++y) {
			Texture::Frame& frame = t->frames[y * columns + x];
			width += frame.width;
			height += frame.height;

			for (int px = 0; px < frame.width; ++px) {
				for (int py = 0; py < frame.height; ++py) {

					int offset = (xoffset + px) + (yoffset + py) * width;

				}
			}
		}
	}

	unsigned char* output = new unsigned char[width * height * 4];

	for (int x = 0; x < columns; ++x) {
		for (int y = 0; y < rows; ++y) {
			Texture::Frame& frame = t->frames[y * columns + x];

			for (int px = 0; px < frame.width; ++px) {
				for (int py = 0; py < frame.height; ++py) {

					int out_offset = ((xoffset + px) + (yoffset + py) * width) * 4;
					int in_offset = (px + py * frame.width) * 4;

					output[out_offset + 0] = frame.buffer[in_offset + 0];
					output[out_offset + 1] = frame.buffer[in_offset + 1];
					output[out_offset + 2] = frame.buffer[in_offset + 2];
					output[out_offset + 3] = frame.buffer[in_offset + 3];

				}
			}

			xoffset += frame.width;
			yoffset += frame.height;
		}
	}

	stbi_write_png("out/background.png", width, height, 4, output, 0);
	delete[] output;

};

// parses a line, splits it by ' ' and then maps a key to a value in format "key"="value"
// quotation marks and the = will be cut
std::map<std::string, std::string> parse_line_arguments(const std::string& line) {
    
    std::vector<std::string> sets = HeStringUtils::split(line, ' ');
    std::map<std::string, std::string> result;
    
    for(const std::string& all : sets) {
        const size_t index = all.find('=');
        if(index != std::string::npos)
            result[all.substr(0, index)] = all.substr(index + 2, all.find_last_of('"') - index - 2);
    }
    
    return result;
    
}

// parses a .tsx file of given source file and parses the information in it. If this is an animated tile_set,
// the animation data is stored in the file, else the first line of the resulting image will simply be blank
void parse_tile_set_source(Texture* t, const std::string& file_name) {
    
    std::ifstream stream(file_name);
    if(!stream) {
        std::cout << "Error: Could not parse TileSetFile [" << file_name << "]: FileNotFound" << std::endl;
        return;
    }
    
    std::string line;
    std::getline(stream, line); // skip first line
    std::getline(stream, line); // info about image format
    auto image_format_info = parse_line_arguments(line);
	t->tile_height = std::stoi(image_format_info["tileheight"]);
    t->tile_width  = std::stoi(image_format_info["tilewidth"]);
    t->tile_count  = std::stoi(image_format_info["tilecount"]);

    std::getline(stream, line);
	HeStringUtils::ltrim(line);
	if (HeStringUtils::startsWith(line, "<image")) {
		image_format_info = parse_line_arguments(line);
		t->tile_columns = std::stoi(image_format_info["width"]) / t->tile_width;
		t->tile_rows = std::stoi(image_format_info["height"]) / t->tile_height;

		Texture::Frame* f = &t->frames.emplace_back();
		std::string source = image_format_info["source"];
		f->buffer = stbi_load(source.c_str(), &f->width, &f->height, &f->channels, 4);
		std::cout << "Debug: Read texture [" << file_name << "] with " << t->frames[0].channels << " channels" << std::endl;
	}
	else {
		// parse all frames and store them in t
		//std::getline(stream, line); // skip grid information line

		while (std::getline(stream, line)) {

			// remove beginning spaces
			HeStringUtils::ltrim(line);
			if (HeStringUtils::startsWith(line, "<tile")) {
				// parse frame
				std::getline(stream, line);
				auto tile_info = parse_line_arguments(line);
				
				Texture::Frame frame;
				frame.width = std::stoi(tile_info["width"]);
				frame.height = std::stoi(tile_info["height"]);

				std::string source = tile_info["source"];
				frame.buffer = stbi_load(source.c_str(), &frame.width, &frame.height, &frame.channels, 4);
				t->frames.emplace_back(frame);

				std::cout << "Debug: Loaded frame of animation with [" << frame.channels << "] channels" << std::endl;

				if (t->frames.size() == 1) {
					// this was the first frame, parse animation data
					std::getline(stream, line); // parse animation init line
					std::getline(stream, line); // parse animation info line
					tile_info = parse_line_arguments(line);
					t->frame_duration = std::stoi(tile_info["duration"]);
					std::getline(stream, line); // parse animation end line
				}

				std::getline(stream, line); // parse tile end line

			}

		}
	}

}

// parses all tile_sets that are defined in the level file
void parse_tile_sets(const HeJsonEntry& tile_root) {

    for(const HeJsonEntry& sets : tile_root.array) {
        
        Texture t;
        t.grid_start = (int) sets["firstgid"];
        
        if(heJsonObjectHasChild(&sets, "source")) {
			std::cout << "Debug: Parsing tile set " << (std::string) sets["source"] << std::endl;
            parse_tile_set_source(&t, ((std::string) sets["source"]));
			if (t.frames.size() > 1) {
				std::cout << "Debug: Creating animation for tile_set [" << (std::string) sets["source"] << "]" << std::endl;
				create_animation_file(&t);
			}
        }
        
		level.tilesets.emplace_back(t);
    }

}

int main(int argc, char* argv[]) {
    
    std::string file_name = "world_file.json";
	
	std::filesystem::create_directory("out");
    std::cout << "Info: Converting file [" << file_name <<"]" << std::endl;
    
    HeJsonEntry root = heJsonLoadDocument(file_name);

	level.height      = (int)root["height"];
	level.width       = (int)root["width"];
	level.tile_height = (int)root["tileheight"];
	level.tile_width  = (int)root["tilewidth"];

    parse_tile_sets(root["tilesets"]);

	parse_background_textures(root["layers"][0]["data"], level.width, level.height);
    
    std::cout << "Info: Successfully converted file" << std::endl;
    
	system("pause");
 
}