#pragma once
#include <vector>
#include <string>
#include <map>

typedef enum HeJsonTokenType {
    HE_JSON_TOKEN_TYPE_NONE,
    HE_JSON_TOKEN_TYPE_CURLY_BRACKET_LEFT,
    HE_JSON_TOKEN_TYPE_CURLY_BRACKET_RIGHT,
    HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_LEFT,
    HE_JSON_TOKEN_TYPE_SQUARE_BRACKET_RIGHT,
    HE_JSON_TOKEN_TYPE_QUOTE,
    HE_JSON_TOKEN_TYPE_COMMA,
    HE_JSON_TOKEN_TYPE_COLON,
    HE_JSON_TOKEN_TYPE_SEMI_COLON,
    HE_JSON_TOKEN_TYPE_STRING,
    HE_JSON_TOKEN_TYPE_NUMBER,
	HE_JSON_TOKEN_TYPE_BOOLEAN
} HeJsonTokenType;

typedef enum HeJsonEntryType {
	HE_JSON_ENTRY_TYPE_NONE,
	HE_JSON_ENTRY_TYPE_BOOLEAN,
    HE_JSON_ENTRY_TYPE_STRING,
    HE_JSON_ENTRY_TYPE_NUMBER,
    HE_JSON_ENTRY_TYPE_OBJECT,
    HE_JSON_ENTRY_TYPE_ARRAY
} HeJsonEntryType;

struct HeJsonToken {
    // the type of this token, decisive of the union value
    HeJsonTokenType  type;
    // the line this token was read from the file
    unsigned int lineIndex;
    
    // only set if type is HE_TOKEN_TYPE_STRING
    std::string _string;
    
    union {
        // only set if type is HE_TOKEN_TYPE_NUMBER
        double      _number;
        // set if type is none of the two above, therefore not an actual value but syntax
		// if type is boolean, this char will be either 't' or 'f'
        char        _token;
    };
    
    HeJsonToken() : type(HE_JSON_TOKEN_TYPE_NONE), _token(' ') {};
    HeJsonToken(const HeJsonTokenType type, const unsigned int line) : type(type), lineIndex(line) {};
    HeJsonToken(const HeJsonToken& token);
    ~HeJsonToken() {};
};

struct HeJsonTokenVector {
    // a list of tokens read from the file
    std::vector<HeJsonToken> tokens;
    // an empty token for reference when reaching out-of-bounds
    HeJsonToken emptyToken;
    // the current index in the vector
    int index = -1;
};

struct HeJsonEntry {
    // the key (left side) of the entry, without quotation
    std::string key = "";
    
    // the type of the value. Depending on this value, different variables will be set and used
    HeJsonEntryType type = HE_JSON_ENTRY_TYPE_NONE;
    
    double                             number = 0.0; // a simple number as string
    std::string                        string = "";  // a simple value as string
    std::vector<HeJsonEntry>           array;        // an array of children 
    std::map<std::string, HeJsonEntry> object;       // maps child objects to their name
    
    HeJsonEntry() {};
    ~HeJsonEntry() {};
    
    // operators
    
    const HeJsonEntry& operator[](const unsigned int index) const; // used for arrays
    const HeJsonEntry& operator[](const std::string& index) const; // used for objects
    
    explicit operator std::string() const { return string; };
    explicit operator double() const      { return number; };
	explicit operator bool() const        { return number == 0.0; };
    explicit operator int() const         { return (int) number; };
};


// returns a string version of given token type
extern std::string heJsonToString(const HeJsonTokenType& type);
// returns either the string, the number as string or the char token of given token depending on its type 
extern std::string heJsonToString(const HeJsonToken& token);
// advances in the vector and returnes the now current token
extern HeJsonToken& heJsonGetNextToken(HeJsonTokenVector* vector);
// returns the token that is next in the vector without advancing
extern HeJsonToken& heJsonPeekNextToken(HeJsonTokenVector* vector);
// returns true if there are still unread tokens in the vector by comparing the current index
// with the size of the token vector
extern inline bool heJsonVectorAvilable(HeJsonTokenVector* vector);

// parses all tokens of the file into the given vector
extern void heJsonParseFile(HeJsonTokenVector* vector, const std::string& fileName);
// parses the vector and returns the root json object (or an empty token if there was an error)
extern inline HeJsonEntry heJsonParseTokenVector(HeJsonTokenVector* vector);

// returns the root object of the given file.
// This tries to load given file, parse the tokens and translate the token vector,
// then returns the root object
extern inline HeJsonEntry heJsonLoadDocument(const std::string& fileName);

// returns true if given entry has a child with the key childName. This function only works
// if entry is an object
extern inline bool heJsonObjectHasChild(const HeJsonEntry* entry, const std::string& childName);

// actual parsing

// starts parsing a value. This value can be a string, number, object or array
extern void heJsonParse(HeJsonTokenVector* vector, HeJsonEntry* parent);
// parses an object recursivly. Parent is the entry which value is an object
extern void heJsonParseObject(HeJsonTokenVector* vector, HeJsonEntry* parent);
// parses an array recursivly. Parent is the entry which value is an array
extern void heJsonParseArray(HeJsonTokenVector* vector, HeJsonEntry* parent);
// gets the key of the next entry without the quotes. Advances in the vector
extern std::string heJsonGetNextEntryKey(HeJsonTokenVector* vector);