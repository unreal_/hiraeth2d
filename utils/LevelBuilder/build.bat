echo Building Release Mode

cd C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\
C:
call "vcvars64.bat"
cd D:\Benutzer\Victor\Textdateien\C++\Hiraeth2D\utils\LevelBuilder
D:

CL.exe src/levelbuilder.cpp src/jsonreader.cpp src/heUtils.cpp ^
 /GS /W1 /Zc:wchar_t /Zi /Gm- /Od /std:c++17 /EHsc ^
 /Fd"D:\Benutzer\Victor\Textdateien\C++\Hiraeth2D\utils\LevelBuilder\bin-int\vc142.pdb" /Zc:inline /fp:precise /errorReport:prompt /WX- /Zc:forScope /Gd /MD /FC /EHsc /nologo ^
 /Fo"D:\Benutzer\Victor\Textdateien\C++\Hiraeth2D\utils\LevelBuilder\bin-int/" /diagnostics:column ^
 /link /out:bin\levelbuilder.exe

echo Running LevelBuilder & echo.
copy bin\levelbuilder.exe D:\Benutzer\Victor\Textdateien\cgseminar\test_tiled_proj\levelbuilder.exe /Y
cd D:\Benutzer\Victor\Textdateien\cgseminar\test_tiled_proj
 .\levelbuilder.exe
pause