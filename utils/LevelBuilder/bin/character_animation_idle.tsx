<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="character_animation_idle" tilewidth="526" tileheight="562" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="416" height="562" source="../prototype/char_idle.png"/>
  <animation>
   <frame tileid="0" duration="33"/>
  </animation>
 </tile>
 <tile id="1">
  <image width="526" height="487" source="../prototype/char_idle_2.png"/>
 </tile>
</tileset>
