<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="fire_animation" tilewidth="256" tileheight="256" tilecount="3" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="256" height="256" source="../animation/frame_1.png"/>
  <animation>
   <frame tileid="0" duration="33"/>
  </animation>
 </tile>
 <tile id="3">
  <image width="256" height="256" source="../animation/frame_2.png"/>
 </tile>
 <tile id="4">
  <image width="256" height="256" source="../animation/frame_3.png"/>
 </tile>
</tileset>
