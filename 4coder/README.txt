Thank you for contributing to the 4coder project!

To submit bug reports or to request particular features email:
editor@4coder.net

Watch the 4coder.handmade.network blog and @AllenWebster4th twitter for news about 4coder progress.

For documentation, feature lists, and usage tutorial videos go to:
4coder.com


