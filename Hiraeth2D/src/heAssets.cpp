#include "heAssets.h"
#include "heUtils.h"
#include <fstream>

HeFontPool HeAssets::fontPool;
HeTexturePool HeAssets::texturePool;

int getCharacterValue(std::vector<std::string>& line, const std::string& key) {

	int value = 0;

	unsigned int index = 0;
	for (const std::string& all : line) {
		if (HeStringUtils::startsWith(all, key)) {
			size_t equalSign = all.find('=');
			std::string svalue = all.substr(equalSign + 1);
			value = std::stoi(svalue);
			line.erase(line.begin() + index);
			break;
		}

		++index;
	}

	return value;

}

void loadPaddingData(HeFont* font, const std::vector<std::string>& list) {

	std::string variable = "";
	for (const std::string& all : list) {
		if (HeStringUtils::startsWith(all, "padding")) {
			variable = all.substr(8);
			break;
		}
	}

	if (variable.empty()) {
		font->padding[0] = 8;
		font->padding[1] = 8;
		font->padding[2] = 8;
		font->padding[3] = 8;
	}
	else {
		std::vector<std::string> numbers = HeStringUtils::split(variable, ',');
		font->padding[0] = std::stoi(numbers[0]);
		font->padding[1] = std::stoi(numbers[1]);
		font->padding[2] = std::stoi(numbers[2]);
		font->padding[3] = std::stoi(numbers[3]);
	}

}


void heLoadFont(HeFont* font, const std::string& fileName) {

	std::ifstream stream(fileName);
	if (!stream) {
		std::cout << "Error: Could not find font file [" << fileName << "]" << std::endl;
		return;
	}

	std::string line;
	std::getline(stream, line);
	std::vector<std::string> infoLine = HeStringUtils::split(line, ' '); // info
	loadPaddingData(font, infoLine);

	std::getline(stream, line);
	std::vector<std::string> commonLine = HeStringUtils::split(line, ' '); // common
	font->textureWidth = getCharacterValue(commonLine, "scaleW");
	font->textureHeight = getCharacterValue(commonLine, "scaleH");
	font->fontSizeInPixels = getCharacterValue(commonLine, "lineHeight");

	std::string textureLine;
	std::getline(stream, textureLine);
	textureLine = textureLine.substr(textureLine.rfind('=') + 2); // remove start quotation
	textureLine.pop_back(); // remove end quotation

	// parse the folder this font file is in so we can search for the texture in the same folder
	std::string folder = "";
	size_t index = fileName.find_last_of('/');
	if(index == std::string::npos)
		index = fileName.find_last_of('\\');
	if (index != std::string::npos)
		folder = fileName.substr(0, index);

	font->texture = heGetTexture(folder + "/" + textureLine);

	const int paddingWidth = font->padding[1] + font->padding[3];
	const int paddingHeight = font->padding[0] + font->padding[2];
	const float lineHeight = (float) font->fontSizeInPixels - paddingHeight;

	while (std::getline(stream, line)) {
		
		std::vector<std::string> args = HeStringUtils::split(line, ' ');
		HeFontCharacter _char;

		const int width = getCharacterValue(args, "width") - (paddingWidth - HeFont::DESIRED_PADDING * 2);
		const int height = getCharacterValue(args, "height") - (paddingHeight - HeFont::DESIRED_PADDING * 2);

		_char.texx = (getCharacterValue(args, "x") + (font->padding[1] - HeFont::DESIRED_PADDING)) / (float)font->textureWidth;
		_char.texy = (getCharacterValue(args, "y") + (font->padding[0] - HeFont::DESIRED_PADDING)) / (float)font->textureHeight;
		_char.texwidth = width / (float)font->textureWidth;
		_char.texheight = height / (float)font->textureHeight;

		_char.width = width / lineHeight;
		_char.height = height / lineHeight;
		_char.xoffset = (getCharacterValue(args, "xoffset") + font->padding[1] - HeFont::DESIRED_PADDING) / lineHeight;
		_char.yoffset = (getCharacterValue(args, "yoffset") + font->padding[0] - HeFont::DESIRED_PADDING) / lineHeight;
		_char.xadvance = (getCharacterValue(args, "xadvance") - paddingWidth) / lineHeight;

		const unsigned int id = getCharacterValue(args, "id");

		font->characters[id] = _char;
	}

}

void heDestroyFont(HeFont* font) {

	if (--font->referenceCount == 0) {
		heDestroyTexture(font->texture);
		font->characters.clear();
		font->fontSizeInPixels = 0;
		font->textureHeight = 0;
		font->textureWidth = 0;
	}

}


HeFont* heGetFont(const std::string& fileName) {

	HeFont* font = nullptr;
	auto it = HeAssets::fontPool.find(fileName);
	if (it != HeAssets::fontPool.end())
		font = &it->second;
	else {
		font = &HeAssets::fontPool[fileName];
		heLoadFont(font, fileName);
	}

	font->referenceCount++;
	return font;

}

HeTexture* heGetTexture(const std::string& fileName) {

	HeTexture* texture = nullptr;
	auto it = HeAssets::texturePool.find(fileName);
	if (it != HeAssets::texturePool.end())
		texture = &it->second;
	else {
		texture = &HeAssets::texturePool[fileName];
		heLoadTexture(texture, fileName);
	}
	
	texture->referenceCount++;
	return texture;

}