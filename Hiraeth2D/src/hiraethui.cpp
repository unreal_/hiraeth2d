#include "heWindow.h"
#include "heGlLayer.h"
#include "heRenderer.h"
#include <thread>

#define TEST_2D

HeWindow window;
HeD2Level level;

void gameThread() {
	HeD2Object* player = &level.objects["player"][0];
	HeD2LightSource* playerLight = &level.lights[1];

	const int sleep = 10;
	const float time = sleep / 1000.0f;
	hm::vec2f player_velocity;



	while (!window.shouldClose) {
		if (window.keyboardInfo.keyStatus[HE_KEY_D])
			player_velocity.x += 200 * time;

		if (window.keyboardInfo.keyStatus[HE_KEY_A])
			player_velocity.x -= 200 * time;

		if (player_velocity.y == 0 && heKeyWasPressed(&window, HE_KEY_SPACE)) {
			// currently standing on shit, just a hack for now
			player_velocity.y -= 8;
		}

		player_velocity.y += 20 * time;

		// pixel perfect collision
		if (HeD2::check_collision(player, &level, player->position + hm::vec2f(player_velocity.x, 0))) {
			float sign = (float) hm::sign(player_velocity.x);
			while (!HeD2::check_collision(player, &level, player->position + hm::vec2f(sign, 0)))
				player->position.x += sign;
			player_velocity.x = 0;
		}


		// pixel perfect collision
		if (HeD2::check_collision(player, &level, player->position + hm::vec2f(0, player_velocity.y))) {
			float sign = (float) hm::sign(player_velocity.y);
			while (!HeD2::check_collision(player, &level, player->position + hm::vec2f(0, sign)))
				player->position.y += sign;
			player_velocity.y = 0;
		}

		player->position += player_velocity;
		player_velocity.x = 0;

		level.camera.position = player->position;
		//playerLight->vector = level.camera.position + hm::vec2f(0.63, 0.43) * player->size;

		heIncreaseAnimationTime(&player->sprite, int(time * 1000));
		heIncreaseAnimationTime(&level.objects["fire"][0].sprite, int(time * 1000));
		
		Sleep(sleep);
	}


}

int main() {
	HeWindowInfo windowInfo;
    windowInfo.title  = L"HirathTest";
    windowInfo.size.x = 1366;
    windowInfo.size.y = 768;
    windowInfo.fpsCap = 80;
    windowInfo.backgroundColour = hm::colour(0, 100, 150);
    
    window.windowInfo = windowInfo;
	if (!heCreateWindow(&window))
		return -1;
    
	heUpdateWindow(&window);
    
	HeRenderEngine engine;
	heCreateRenderEngine(&engine, &window);
	hePrepareUiEngine(&engine);
    
	// read textures in main thread
	//heGetTexture("res/sprites/char_idle.png");
	//heGetTexture("res/sprites/ground.png");


	 // TEST UI CODE
#ifdef TEST_UI
		HeColourInput input(HE_COLOUR_INPUT_TYPE_SOLID_COLOUR);
		input.solid.colour = hm::colour(255, 255, 255);

		HeColourInput input2(HE_COLOUR_INPUT_TYPE_SOLID_COLOUR);
		input2.solid.colour = hm::colour(0);

		HeColourInput input3(HE_COLOUR_INPUT_TYPE_TEXTURE);
		input3.texture.texture = heGetTexture("test.jpg");

		HeColourInput transition(HE_COLOUR_INPUT_TYPE_TRANSITION);
		transition.transition.steps.emplace_back(input);
		transition.transition.steps.emplace_back(input2);
		transition.transition.currentTime = 1.0;



		HeShape shape;
		shape.position = heCreateConstraint(hm::vec2f(0.1f), window.windowInfo.size, HE_CONSTRAINT_TYPE_WINDOW_SPACE, HE_CONSTRAINT_FLAG_KEEP_RATIO);
		shape.size = heCreateConstraint(hm::vec2f(0.5, 0.5), window.windowInfo.size, HE_CONSTRAINT_TYPE_WINDOW_SPACE, HE_CONSTRAINT_FLAG_KEEP_RATIO);
		shape.type = HE_SHAPE_TYPE_RECTANGLE;

		HeWidget widget(HE_WIDGET_TYPE_SHAPE);
		widget.input = transition;
		widget.shape = shape;



		HeFont* font = heGetFont("res/fonts/main.fnt");
		HeWidget textWidget(HE_WIDGET_TYPE_TEXT);
		textWidget.input = transition;
		heGetText(&textWidget.text, font, "This is a test text");
		textWidget.text.position = heCreateWindowConstraintFromPixelSpace(hm::vec2f(200, 200), window.windowInfo.size, HE_CONSTRAINT_FLAG_RESIZE);
		textWidget.text.scale = heCreateWindowConstraintFromPixelSpace(hm::vec2f(30), window.windowInfo.size, HE_CONSTRAINT_FLAG_KEEP_RATIO);
		textWidget.depth = 1;

		float time = 0;
		float textScale = 0.01f;
#endif
	

#ifdef TEST_2D
	/*
	level.camera.position = hm::vec2f(500, 500);
	level.camera.zoom = 0;
	level.viewSize = window.windowInfo.size;
	
	for (int x = 0; x < 10; ++x) {
		HeD2Object* ground = &level.objects["ground"].emplace_back();
		ground->size = hm::vec2i(220, 200);
		ground->position = hm::vec2i(x * ground->size.x, 600);
		heLoadSprite(&ground->sprite, "res/sprites/ground.h2s");
	}

	HeD2Object* fire = &level.objects["fire"].emplace_back();
	fire->size = hm::vec2i(100, 100);
	fire->position = hm::vec2i(500, 500);
	heLoadSprite(&fire->sprite, "res/sprites/sfire.h2s");

	HeD2Object* player = &level.objects["player"].emplace_back();
	player->size = hm::vec2i(460 * 0.2, 580 * 0.2);
	player->position = hm::vec2i(100, 300);
	heLoadSprite(&player->sprite, "res/sprites/char_idle.h2s");
	*/

	heLoadLevel(&level, "res/levels/level.h2l", "res/sprites/"); 
	level.viewSize = hm::vec2(1366, 768);
	
	HeD2LightSource& l = level.lights.emplace_back();
	l.type             = HE_D2_LIGHT_SOURCE_TYPE_DIRECTIONAL;
	l.colour           = hm::colour(120, 120, 200, 255 * 2);
	l.vector           = hm::vec2f(1);
	
	HeD2LightSource& l1 = level.lights.emplace_back();
	l1.type             = HE_D2_LIGHT_SOURCE_TYPE_POINT;
	l1.colour           = hm::colour(247, 117, 33, 255 * 5);
	l1.vector           = hm::vec2f(400, 780);
	l1.data[0]          = 256 * 256;
	
	std::thread game(gameThread);
#endif


	while (!window.shouldClose) {
		heUpdateWindow(&window);
        
#ifdef TEST_UI
			
			// rendering stuff
			if (widget.status & HE_WIDGET_STATUS_HOVERED) {
				time += (float) window.frameTime * 3.0f;
				if (time > 1.0f)
					time = 1.0f;
			} else {
				time -= (float) window.frameTime * 3.0f;
				if (time < 0.0f)
					time = 0.0f;
			}

			widget.input.transition.currentTime = time;
			textWidget.input.transition.currentTime = 1.0 - time;

			//if (heUiPushWidget(&engine, &widget))
				//std::cout << "YeahBoi" << std::endl;

			heUiPushWidget(&engine, &textWidget);
		

			heRenderUiQueue(&engine);
		
#endif

#ifdef TEST_2D
		heRenderD2Level(&engine, &level);
#endif

        heSwapWindow(&window);
        heSyncToFps(&window);
	}
    
#ifdef TEST_2D
	game.detach();
#endif
	
	heFinishEngine(&engine);
	heDestroyRenderEngine(&engine);
	heDestroyWindow(&window);
    return 0;
}