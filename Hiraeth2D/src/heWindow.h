#pragma once
#include <windows.h>
#include <string>
#include "hm/hm.hpp"
#include "heShapes.h"

enum HeKeyCode {
	HE_KEY_0 = 0x030,
	HE_KEY_1 = 0x031,
	HE_KEY_2 = 0x032,
	HE_KEY_3 = 0x033,
	HE_KEY_4 = 0x034,
	HE_KEY_5 = 0x035,
	HE_KEY_6 = 0x036,
	HE_KEY_7 = 0x037,
	HE_KEY_8 = 0x038,
	HE_KEY_9 = 0x039,
	HE_KEY_A = 0x041,
	HE_KEY_B = 0x042,
	HE_KEY_C = 0x043,
	HE_KEY_D = 0x044,
	HE_KEY_E = 0x045,
	HE_KEY_F = 0x046,
	HE_KEY_G = 0x047,
	HE_KEY_H = 0x048,
	HE_KEY_I = 0x049,
	HE_KEY_J = 0x04A,
	HE_KEY_K = 0x04B,
	HE_KEY_L = 0x04C,
	HE_KEY_M = 0x04D,
	HE_KEY_N = 0x04E,
	HE_KEY_O = 0x04F,
	HE_KEY_P = 0x050,
	HE_KEY_Q = 0x051,
	HE_KEY_R = 0x052,
	HE_KEY_S = 0x053,
	HE_KEY_T = 0x054,
	HE_KEY_U = 0x055,
	HE_KEY_V = 0x056,
	HE_KEY_W = 0x057,
	HE_KEY_X = 0x058,
	HE_KEY_Y = 0x059,
	HE_KEY_Z = 0x05A,
	HE_KEY_CAPS = 0x14,
	HE_KEY_SPACE = 0x20,
	HE_KEY_LSHIFT = 0xA0,
	HE_KEY_RSHIFT = 0xA1,
	HE_KEY_ESCAPE = 0x1B,
	HE_KEY_ENTER = 0x0D,
	HE_KEY_BACKSPACE = 0x08,
	HE_KEY_F1 = 0x70,
	HE_KEY_F2 = 0x71,
	HE_KEY_F3 = 0x72,
	HE_KEY_F4 = 0x73,
	HE_KEY_F5 = 0x74,
	HE_KEY_F6 = 0x75,
	HE_KEY_F7 = 0x76,
	HE_KEY_F8 = 0x77,
	HE_KEY_F9 = 0x78,
	HE_KEY_F10 = 0x79,
	HE_KEY_F11 = 0xFA,
	HE_KEY_F12 = 0x7B
};

struct HeWindowInfo {
	// the background colour of the window
    hm::colour   backgroundColour;
	// the size of the window, in pixels
	hm::vec2i    size;
	// the name of the window
    std::wstring title  = L"";
	// maximum fps allowed. If this is set to 0, vsync will be enabled
	unsigned int fpsCap = 0;
};

struct HeMouseInfo {
	HeConstraint mousePosition;
	bool leftButtonDown = false;
	bool rightButtonDown = false;
};

struct HeKeyboardInfo {
	// maps every key to their status, if a key is down, the value will be true, if the key is not down
	// the value will be true
	std::map<HeKeyCode, bool> keyStatus;
	// a vector of all keys that were pushed down in the last frame, useful for i.e. typing. This vector is cleard in
	// the heUpdateWindow function
	std::vector<HeKeyCode> keysPressed;
};

struct HeWindow {
	// information
	HeKeyboardInfo keyboardInfo;
    HeWindowInfo   windowInfo;
	HeMouseInfo    mouseInfo;
    bool           shouldClose = false;
    
    // opengl stuff
    HGLRC        context = nullptr;
    HWND         handle  = nullptr;
    HDC          dc      = nullptr;
    
    // timing stuff
    double lastFrame = 0.; // the last frame time (time_since_epoch)
    double frameTime = 0.; // the duration of the last frame (in seconds)
};

// creates the windows class with name Hiraeth2D if it wasnt created before
extern HE_API bool heSetupClassInstance();
// creates a dummy context needed for retrieving function pointers (context creation with attributes...)
extern HE_API void heCreateDummyContext();
// creates the window
extern HE_API bool heCreateWindow      (HeWindow* window);
// updates the input of the window and clears the buffer
extern HE_API void heUpdateWindow      (HeWindow* window);
// destroys the window and its context
extern HE_API void heDestroyWindow     (HeWindow* window);
// sleeps in the current thread until the requested fps cap is reached
extern HE_API void heSyncToFps         (HeWindow* window);
// enables vsync. Should only be called once. Timestamp is the number of frames to wait before doing the next
// one. Should always be one. Called when the window is created and fpsCap is 0
extern HE_API void heEnableVsync       (unsigned int timestamp);
// swaps the buffers of the given window. Should be called after rendering the frame
extern HE_API void heSwapWindow        (const HeWindow* window);
// calculates the window border sizes (caption bar...) 
extern HE_API hm::vec2i heCalculateWindowBorderSize(const HeWindow* window);

// returns true if given key was pressed during the last frame on given window.
// This simply searches the vector of keys pressed of the window's keyboardInfo for key
extern inline HE_API bool heKeyWasPressed(const HeWindow* window, const HeKeyCode key);

// the windows class instance
extern HINSTANCE classInstance;
