#include "heD2.h"
#include <fstream>
#include "heUtils.h"
#include "stb_image.h"

hm::vec4f heGetSpriteAnimationFrame(const HeSpriteComponent* sprite) {

	if (sprite->totalAnimationTime == 0)
		// no animation in this sprite, display whole texture
		return hm::vec4f(0, 0, 1, 1);

	auto iterator = sprite->frames.begin();
	const HeSpriteAnimationFrame* frame = &iterator->second;
	++iterator;

	for (; iterator != sprite->frames.end(); ++iterator) {
		// break if the current frame starts after the current time (the previous frame was correct)
		if (sprite->currentAnimationTime < (int) iterator->first)
			break;
		else
			frame = &iterator->second;
	}

	if (frame == nullptr) {
		// the last frame is the active frame
		frame = &sprite->frames.rbegin()->second;
	}

	if (frame != nullptr) {
		return hm::vec4f(frame->offset.x, frame->offset.y, frame->size.x, frame->size.y);
	} else {
		std::cout << "Error: Could not get a valid frame for sprite component" << std::endl;
		return hm::vec4f(0);
	}

}

void heIncreaseAnimationTime(HeSpriteComponent* sprite, const int time) {

	sprite->currentAnimationTime += time;
	if (sprite->currentAnimationTime > sprite->totalAnimationTime)
		sprite->currentAnimationTime -= (sprite->totalAnimationTime + time);

}

bool heCollidesWith(const HeD2Object* object, const hm::vec2f& point) {

	hm::vec2f center = object->position + object->size / 2;
	hm::vec2f p0 = object->position;
	hm::vec2f p1 = p0 + hm::vec2f((float) object->size.x, 0);
	hm::vec2f p2 = p0 + object->size;
	hm::vec2f p3 = p0 + hm::vec2f(0, (float) object->size.y);

	if (object->rotation != 0.0f) {
		p0 = hm::rotate(p0, center, object->rotation);
		p1 = hm::rotate(p1, center, object->rotation);
		p2 = hm::rotate(p2, center, object->rotation);
		p3 = hm::rotate(p3, center, object->rotation);
	}

	return hm::pointInRectangle(p0, p1, p2, p3, point);

}


namespace HeD2 {

	// maps a flag to its id. This is only used for custom flags. The id for a flag is calculated by shifting
	// a bit by the number of flags already existing. The third flag i.e would be 0b100 (4). The foruth flag
	// would be 0b1000 (8)
	std::map<std::string, unsigned int> custom_flags = {
		{"collidable", HE_D2_OBJECT_FLAG_COLLIDABLE},
		{"animated", HE_D2_OBJECT_FLAG_ANIMATED},
		{"noshadows", HE_D2_OBJECT_FLAG_NO_SHADOWS}
	};

	bool place_free(const HeD2Object* object, const HeD2Level* level, const hm::vec2f& point) {

		bool found_obj = false;

		for (const auto& all : level->objects) {
			for (const auto& objs : all.second) {
				// make sure the object were checking is not the same object were currently colliding with
				// also make sure the current object is collidable
				if (&objs != object && objs.flags & HE_D2_OBJECT_FLAG_COLLIDABLE) {
					if (heCollidesWith(&objs, point)) {
						found_obj = true;
						break;
					}
				}
			};
		};

		// return true if the place is free -> no collision
		return !found_obj;

	};

	bool check_collision(const HeD2Object* object, const HeD2Level* level, const hm::vec2f& point) {

		hm::vec2i p0(object->position);
		hm::vec2i p1(p0 + hm::vec2i(object->size.x, 0));
		hm::vec2i p2(p0 + object->size);
		hm::vec2i p3(p0 + hm::vec2i(0, object->size.y));

		bool found_collision = false;

		for (const auto& types : level->objects) {
			for (const auto& objs : types.second) {
				// dont check given object and only check collidable objects
				if (&objs != object && objs.flags & HE_D2_OBJECT_FLAG_COLLIDABLE) {
					if (object->rotation == 0.0f) {

						bool collisionX = point.x + object->size.x >= objs.position.x &&
							objs.position.x + objs.size.x >= point.x;
						bool collisionY = point.y + object->size.y >= objs.position.y &&
							objs.position.y + objs.size.y >= point.y;

						if (collisionX && collisionY) {
							found_collision = true;
							break;
						}

					} else {
						hm::vec2i r0(objs.position);
						hm::vec2i r1(r0 + hm::vec2i(objs.size.x, 0));
						hm::vec2i r2(r0 + objs.size);
						hm::vec2i r3(r0 + hm::vec2i(0, objs.size.y));

						if (hm::pointInRectangle(r0, r1, r2, r3, p0) ||
							hm::pointInRectangle(r0, r1, r2, r3, p1) ||
							hm::pointInRectangle(r0, r1, r2, r3, p2) ||
							hm::pointInRectangle(r0, r1, r2, r3, p3)) {
							found_collision = true;
							break;
						}
					}
				}
			}

			if (found_collision)
				break;
		}

		return found_collision;

	};

	unsigned int get_flag(const std::string& name) {
		
		auto it = custom_flags.find(name);
		if (it != custom_flags.end())
			return it->second;

		unsigned int id = 0b1;
		id = id << (unsigned int)custom_flags.size();

		custom_flags[name] = id;
		return id;

	};

};


void heLoadSprite(HeSpriteComponent* component, const std::string& spriteFile) {

	std::ifstream stream(spriteFile, std::ios::binary);
	if (!stream) {
		std::cout << "Error: Could not find sprite file [" << spriteFile << "]" << std::endl;
		return;
	}

	std::streampos begin, end;
	begin = stream.tellg();
	stream.seekg(0, std::ios::end);
	end = stream.tellg();
	stream.seekg(0, std::ios::beg);

	std::string line;
	std::getline(stream, line);

	bool isAnimatedTile = HeStringUtils::startsWith(line, "@h2");

	if(!isAnimatedTile)
		stream.seekg(0, std::ios::beg); // revert the first line, this belongs to the texture

	component->texture = &HeAssets::texturePool[spriteFile];

	std::vector<char> content(static_cast<size_t>(end - begin));
	stream.read(&content[0], end - begin);
	stream.close();

	stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load_from_memory(reinterpret_cast<unsigned char*>(&content[0]), (int) content.size(), 
		&component->texture->width, &component->texture->height, &component->texture->channels, 4);

	if (!data) {
		std::cout << "Error: Could not read sprite content: " << stbi_failure_reason() << std::endl;
		return;
	}

	if (isAnimatedTile) {
		// parse animation data

		// split line by comma, cut first 4 chars ('@h2 ')
		std::vector<std::string> arguments = HeStringUtils::split(line.substr(4), ',');
		unsigned int frameWidth = std::stoi(arguments[0]);
		unsigned int frameHeight = std::stoi(arguments[1]);
		unsigned int frameCount = std::stoi(arguments[2]);
		unsigned int columns = std::stoi(arguments[3]);
		unsigned int frameTime = std::stoi(arguments[4]);

		component->currentAnimationTime = 0;
		component->totalAnimationTime = frameCount * frameTime;

		float texFrameWidth = frameWidth / (float)component->texture->width;
		float texFrameHeight = frameHeight / (float)component->texture->height;

		for (unsigned int i = 0; i < frameCount; ++i) {
			unsigned int x = i % columns;
			unsigned int y = i / columns;

			component->frames[i * frameTime] = {
				hm::vec2f(x * texFrameWidth, y * texFrameHeight), // offset
				hm::vec2f(texFrameWidth, texFrameHeight) // size
			};
		}

	} 

	heCreateTexture(data, &component->texture->textureId, component->texture->width, component->texture->height, 
		component->texture->channels);

}

void heLoadLevel(HeD2Level* level, const std::string& levelFile, const std::string& spriteFolder) {

	std::ifstream stream(levelFile);
	if (!stream) {
		std::cout << "Error: Could not find level file [" << levelFile << "]" << std::endl;
		return;
	}

	std::string sizeLine;
	std::getline(stream, sizeLine);
	std::vector<std::string> size = HeStringUtils::split(sizeLine, ',');
	level->levelSize = hm::vec2i(std::stoi(size[0]), std::stoi(size[1]));

	std::string backgroundTexture;
	std::getline(stream, backgroundTexture);
	level->background = heGetTexture(spriteFolder + backgroundTexture);

	// parse objects
	std::string objLine;
	while (std::getline(stream, objLine)) {
		std::vector<std::string> objInfo = HeStringUtils::split(objLine, ',');
		std::string type = objInfo[0];
		int x = std::stoi(objInfo[1]);
		int y = std::stoi(objInfo[2]);
		int w = std::stoi(objInfo[3]);
		int h = std::stoi(objInfo[4]);
		std::string texture = objInfo[5];

		HeD2Object* obj = &level->objects[type].emplace_back();
		obj->position = hm::vec2f((float) x, (float) y);
		obj->size = hm::vec2f((float)w, (float)h);
		heLoadSprite(&obj->sprite, spriteFolder + texture);

		for (int i = 6; i < objInfo.size(); ++i)
			// parse possible flags
			obj->flags |= HeD2::get_flag(objInfo[i]);
	}

}