#pragma once
#include "heShapes.h"
#include "heGui.h"
#include "heGlLayer.h"
#include "heWindow.h"
#include "heD2.h"


// This helper struct only wraps a widgets. Because users should be able to render widgets that are created in a larger scope
// (as class members) as well as create them on the stack (simply render a shape with ColourInput without storing them
// somewhere, immediate style), we need to be able to store both pointers to widgets and "normal" widgets
struct HeWidgetPointer {
	bool isPointer = false;
	HeWidget* ptr = nullptr;
	HeWidget  wdg;
    
	HeWidgetPointer(HeWidget* widget);
	HeWidgetPointer(const HeWidget& widget);
};

typedef std::vector<HeWidgetPointer> HeUiRenderQueue;

struct HeRenderEngine {
	// the shader used for all ui stuff
	HeShaderProgram guiShader;
	// the shader used for all sprites (2d stuff)
	HeShaderProgram spriteShader;
	// the render queue for ui stuff. When you push a ui widget, it gets added to this queue. All widgets in this
	// queue will then be rendered by calling heRenderUiQueue. The queue will be cleared after rendering everything
	HeUiRenderQueue queue;
	// a quadratic vao of scale 1 (whole gl screen)
	HeVao quadVao;
	

	/* d2 stuff */

	// all shadow casters (objects) are rendered in this fbo
	HeFbo d2Fbo;
	// holds the shadow map
	HeFbo shadowFbo;
	// the shader used for rendering the shadow map
	HeShaderProgram shadowShader;
	// mixing the shadow map with the diffuse textures
	HeShaderProgram finalShader;

	// the window this engine runs on (mainly used for constraints)
	HeWindow* window = nullptr;

	// maps a string (text of automatic widget, i.e. button) to the transition progress. If this widget is hovered,
	// the transition time is increased, else is it decreases (and always clamped between [0:1])
	std::map<size_t, double> widgetTransitionTimes;

	/* default settings */ 
	
	// This may be set by the client for control over default behaviour
	// this widget template is used only when pushing buttons directly (heUiPushButton) so that default buttons
	// are easier to handle. If the input of this widget is a transition, the time of the transition will be handled
	// automatically
	HeWidget defaultButtonWidget;
	// the default text colour for buttons when not hovered
	hm::colour defaultButtonColour;
	// default font of the engine use by i.e. text and buttons. Can be set by the client for control over default
	// behaviour. This font should be a pointer to a font in the HeFontPool
	HeFont* defaultFont;
};

// sets up given engine (the shader and vao). Must be called after the window is created but before something is rendered
extern HE_API void heCreateRenderEngine(HeRenderEngine* engine, HeWindow* window);
// destroys given render engine
extern HE_API void heDestroyRenderEngine(HeRenderEngine* engine);
// resizes all fbos of given render engine
extern HE_API void heResizeRenderEngine(HeRenderEngine* engine);
// prepares given render engine for ui rendering (binding the ui shader / vao...)
extern HE_API void hePrepareUiEngine(HeRenderEngine* engine);
// prepares given render engine for sprite rendering (binding the sprite shader / vao...)
// you can only prepare the engine for one level at a time, because different levels will have different cameras ...
// this will load the projection matrix for the camera of given level.
// scale is the scale of the level to window (relative).
// this will update the levels cameras projection matrix
extern HE_API void hePrepareSpriteEngine(HeRenderEngine* engine, HeD2Level* level, const hm::vec2f& scale);
// finishes the render engine (unbinding the shader / vao)
extern HE_API void heFinishEngine(HeRenderEngine* engine);
// renders a given widget. A widget can be either a text or a shape, so this function only either calls
// heRenderShape or heRenderText
extern HE_API void heRenderWidget(HeRenderEngine* engine, HeWidget* widget);
// renders given shape with given colour input using the engines shader
extern HE_API void heRenderShape(HeRenderEngine* engine, const HeShape* shape, const HeColourInput* input,
	const float depth);
// renders given text with given colour input using the engines shader. If the text wasnt created yet (the vao is null)
// it will be loaded now. This is useful for when were pushing things outside of the main context
extern HE_API void heRenderText(HeRenderEngine* engine, HeText* text, const HeColourInput* input, const float depth);
// renders a texture at given position with the textures width and height. This assumes that the quad vao and
// sprite shader are bound and a projection matrix is loaded to it. The position is the top left corner of the texture
// scale is the relative scale of the texture. A scale of (1,1) means that the size on screen will be the pixel size
// of the texture, a scale of (0.5,0.5) will mean that the texture on screen will have the size of the original texture
extern HE_API void heRenderTexture(HeRenderEngine* engine, HeTexture* texture, const hm::vec2i& position, 
	const hm::vec2f& scale);
// renders a (gl) texture from memory onto the screen. The texture will  not be rotated, scaled or transformed. 
// This is used for lighting. texture will be bound to the first gl slot, a shader and a vao is assumed to
// be bound
extern HE_API void heRenderTexture(HeRenderEngine* engine, const unsigned int texture);
// renders given object. This will take the default quad vao of the engine and calculate the current animation frame.
// This will also set the depth of the d2 object.
// scale is the relative scale of this object in relation to the room's view size and the current display size.
// If the objects actual size on screen should be the same as set (HeD2Object#size), set the scale to 1
extern HE_API void heRenderD2Object(HeRenderEngine* engine, HeD2Object* object, const hm::vec2f& scale);
// renders all widgets / sprites in the queue of given render engine. Should normally called once per frame
extern HE_API void heRenderUiQueue(HeRenderEngine* engine);
// renders given room using given render engine
extern HE_API void heRenderD2Level(HeRenderEngine* engine, HeD2Level* level);


// gets the current input information from given window and checks which ones apply to the given widget. Usually called when
// a widget is 'done'
extern HE_API void heUpdateWidgetInput(HeWindow* window, HeWidget* widget);
// adds this widget to the render queue and updates the widget
extern HE_API bool heUiPushWidget(HeRenderEngine* engine, HeWidget* widget);
// adds this widget to the render queue. Returns true if the widget was clicked in the last frame
// This will only add a copy of the widget to the queue, so it will not be updated. Useful only when creating
// widgets in scope
extern HE_API bool heUiPushWidget(HeRenderEngine* engine, HeWidget& widget);
// adds a button with given information to the render queue. Returns true if the button is currently clicked
extern HE_API bool heUiPushShape(HeRenderEngine* engine, const HeShape& shape, const HeColourInput& input);
// adds a new text with given font and colour to the render queue. If font is nullptr, the engine's default font
// will be used
extern HE_API void heUiPushText(HeRenderEngine* engine, HeFont* font, const std::string& text, const hm::colour& colour);
// pushes a new button with given text at given position. This will copy the default button template of the engine
// and use the default font of the engine to create a button. The transition progres is stored in the engine as well,
// using the position as a hash
extern HE_API bool heUiPushButton(HeRenderEngine* engine, const std::string& text, const HeConstraint& position);
