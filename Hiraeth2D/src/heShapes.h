#pragma once
#include "hm/hm.hpp"
#include "heAssets.h"

typedef enum HeShapeType {
    HE_SHAPE_TYPE_RECTANGLE,
    HE_SHAPE_TYPE_ELLIPSE,
	HE_SHAPE_TYPE_TRIANGLE,
	HE_SHAPE_TYPE_TEXT
} HeShapeType;

typedef enum HeConstraintType {
	HE_CONSTRAINT_TYPE_WINDOW_SPACE, // the window space as percentage (like gl), range [0:1] for both x and y
	HE_CONSTRAINT_TYPE_PIXEL_SPACE,  // in pixel space, in range of [0:window_width] / [0:window_height]
} HeConstraintType;

typedef enum HeConstraintFlag {
	// resize this constraint according to the window size (only valid if in window_space)
	HE_CONSTRAINT_FLAG_RESIZE             = 0x001, 
	// ignore window resizing, always keep the original size (always set for pixel_space)
	HE_CONSTRAINT_FLAG_KEEP_ABSOLUTE_SIZE = 0x010, 
	// always keep the original aspect ratio, i.e. ignore horizontal resizing if no vertical resizing was done 
	// (only valud if in window_space)
	HE_CONSTRAINT_FLAG_KEEP_RATIO         = 0x100  
} HeConstraintFlag;

struct HeConstraint {
	hm::vec2f vector;
	hm::vec2f nativeWindowSize;
	HeConstraintType type  = HE_CONSTRAINT_TYPE_PIXEL_SPACE;
	HeConstraintFlag flags = HE_CONSTRAINT_FLAG_RESIZE;
};

struct HeShape {
    HeConstraint position;
    HeConstraint size;
    HeShapeType type = HE_SHAPE_TYPE_RECTANGLE;
	float rotation = 0.f;
};

struct HeText {
	// the rotation of this text around the z axis, in degrees
	float rotation = 0;
	// the vao of this text, created in heLoadText
	HeVao* vao = nullptr;
	HeFont* font = nullptr;
	HeConstraint scale;
	HeConstraint position;
	// the actual string of text this text represents
	std::string string;
};


typedef std::unordered_map<HeFont*, std::unordered_map<std::string, HeVao>> HeTextPool;

namespace HeAssets {
    extern HeTextPool textPool;
};


struct HeWindow;

// loads given text into memory. A text should only be loaded once for better performance.  
extern HE_API void heLoadText(HeText* text, HeFont* font, const std::string& string);
// deletes the vao of the text, making it impossible for rendering this text. The other attributes of the text will not
// be changed however, as they may be needed again later
extern HE_API void heDestroyText(HeText* text);
// returns a text with given text from the text pool
extern HE_API void heGetText(HeText* text, HeFont* font, const std::string& string);

// this allows constraints to be divided by each other. For this to work correctly, both constraints should
// be in the same space. The resulting constraint will have all attributes from the left constraint, but the vector
// will be the result of the division of l's vector and r's vector
extern HE_API HeConstraint operator/(const HeConstraint& l, const HeConstraint& r);
// divides l's vector by r and returns the result as constraint
extern HE_API HeConstraint operator/(const HeConstraint& l, const hm::vec2f& r);

// returns true if position is inside the shape
extern HE_API bool hePointInShape(const HeWindow* window, const HeShape* shape, const HeConstraint& position);
// creates a window_space constraint from given position. WindowSize should be the "expected" window size, so that the relative
// size (in window_space) will be correct. I.e. when creating a ui for a full hd window (1920x1080) and the shape should cover
// half the screen, give 960x540 as vector and 1920x1080 as windowSize.
// vector can be anything window related, such as a position or size etc.
extern HE_API HeConstraint heCreateWindowConstraintFromPixelSpace(const hm::vec2f& vector, const hm::vec2i& windowSize, const HeConstraintFlag flags);
// creates a pixel_space constraint from given vector. windowSize should be the "expected" window size. This is will automatically
// set the HE_CONSTRAINT_FLAG_KEEP_ABSOLUTE_SIZE flag for this constraint.
extern HE_API HeConstraint heCreatePixelConstraintFromWindowSpace(const hm::vec2f& vector, const hm::vec2i& windowSize);
// creates a new constraint with given parameters. vector is the vector of the constraint, which can be in both window space and
// pixel space, depending on type. windowSize is the "expected" windowSize, only used when using the HE_CONSTRAINT_FLAG_KEEP_RATIO
// or HE_CONSTRAINT_FLAG_KEEP_ABSOLUTE_SIZE. Note that these two flags only function when in window_space.
// Constraints in pixel_space will always keep their absolute sizes, only window_space constraints will change size
// (if HE_CONSTRAINT_FLAG_KEEP_ABSOLUTE_SIZE is not set)
extern HE_API HeConstraint heCreateConstraint(const hm::vec2f& vector, const hm::vec2i& windowSize, const HeConstraintType type, const HeConstraintFlag flags);
// returns the vector of the constraint (size, position...) in the requested type (window_space, pixel_space...)
extern HE_API hm::vec2f heGetConstraint(const HeConstraint* constraint, const HeConstraintType targetType, const HeWindow* window);