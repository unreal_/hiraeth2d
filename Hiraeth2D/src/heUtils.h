#pragma once
#include <string>
#include <vector>

#ifdef HE_EXPORTS
#define HE_API __declspec(dllexport)
#else
#define HE_API __declspec(dllimport)
#endif


namespace HeStringUtils {
	/* checks whether base starts with check */
	extern HE_API bool startsWith(const std::string& base, const std::string& check);
	/* checks whether base ends with the string check */
	extern HE_API bool endsWith(const std::string& base, const std::string& check);
	/* trim from start (in place) */
	extern HE_API void ltrim(std::string& s);
	/* trim from end (in place) */
	extern HE_API void rtrim(std::string& s);
	/* trim from both ends (in place) */
	extern HE_API void trim(std::string& s);
	/* splits the given string by delimiter and stores it in result after clearing */
	extern HE_API void split(const std::string& s, const char& delim, std::vector<std::string>& result);
	/* checks wether the two given strings are the same */
	extern HE_API bool equals(const std::string& s1, const std::string& s2);
	/* checks wether the given string is completely alpha numeric */
	extern HE_API bool isAlphaNumeric(std::string& s);
	/* splits the given string by delimiter and returns the single splits */
	extern HE_API std::vector<std::string> split(const std::string& s, const char& delim);
	/* replaces all occurences of from into to in input */
	extern HE_API std::string replaceAll(const std::string& input, const std::string& from, const std::string& to);
	/* counts how many times character occurres in input */
	extern HE_API unsigned int count(const std::string& input, const char character);
};
