#include "heShapes.h"
#include "heWindow.h"


HeTextPool HeAssets::textPool;


void heLoadText(HeText* text, HeFont* font, const std::string& string) {
    
	std::vector<float> vertices, textures;
    
	hm::vec2f position(0.f);
	float fontSize = 2.0f;
    
	unsigned int length = (unsigned int)string.size();
	for (unsigned int i = 0; i < length; ++i) {
		HeFontCharacter* _char = &font->characters[string.at(i)];
        
		if ((int)string.at(i) == 32) {
			// space
			position.x += _char->xadvance * fontSize;
			continue;
		}
        
		hm::vec2f size = { _char->width * fontSize, _char->height * fontSize };
		hm::vec2f offset = { _char->xoffset * fontSize, _char->yoffset * fontSize };
        
		hm::vec2f p1{ position.x + 0.f * size.x + offset.x, position.y + 0.f * size.y + offset.y };
		hm::vec2f p2{ position.x + 0.f * size.x + offset.x, position.y + 1.f * size.y + offset.y };
		hm::vec2f p3{ position.x + 1.f * size.x + offset.x, position.y + 1.f * size.y + offset.y };
		hm::vec2f p4{ position.x + 1.f * size.x + offset.x, position.y + 0.f * size.y + offset.y };
        
		vertices.emplace_back(p1.x);
		vertices.emplace_back(p1.y);
		vertices.emplace_back(p3.x);
		vertices.emplace_back(p3.y);
		vertices.emplace_back(p4.x);
		vertices.emplace_back(p4.y);
        
		vertices.emplace_back(p1.x);
		vertices.emplace_back(p1.y);
		vertices.emplace_back(p2.x);
		vertices.emplace_back(p2.y);
		vertices.emplace_back(p3.x);
		vertices.emplace_back(p3.y);
        
		hm::vec2f t1(_char->texx, _char->texy);
		hm::vec2f t2(t1 + hm::vec2f(0, _char->texheight));
		hm::vec2f t3(t1 + hm::vec2f(_char->texwidth, _char->texheight));
		hm::vec2f t4(t1 + hm::vec2f(_char->texwidth, 0));
        
		textures.emplace_back(t1.x);
		textures.emplace_back(-t1.y);
		textures.emplace_back(t3.x);
		textures.emplace_back(-t3.y);
		textures.emplace_back(t4.x);
		textures.emplace_back(-t4.y);
        
		textures.emplace_back(t1.x);
		textures.emplace_back(-t1.y);
		textures.emplace_back(t2.x);
		textures.emplace_back(-t2.y);
		textures.emplace_back(t3.x);
		textures.emplace_back(-t3.y);
        
		position.x += (_char->xadvance * fontSize);
	}
    
	text->font = font;
	heCreateVao(text->vao);
	heBindVao(text->vao);
	heAddVaoData(text->vao, vertices, 2);
	heAddVaoData(text->vao, textures, 2);
	heUnbindVao(text->vao);
    
}

void heDestroyText(HeText* text) {
    
    heDestroyVao(text->vao);
        
    // try to remove from font pool
        
    auto font = HeAssets::textPool.find(text->font);
    if(font != HeAssets::textPool.end()) {
        auto& map = HeAssets::textPool[text->font];
			
		size_t offset = 0;
		for (auto it : map) {
			if (&it.second == text->vao) {
				heDestroyVao(&it.second);
				std::string s = (it.first);
				map.erase(map.find(s));
			}

			++offset;
		}
    }

}

void heGetText(HeText* text, HeFont* font, const std::string& string) {
    
	text->vao = nullptr;

    auto fontIt = HeAssets::textPool.find(font);
    if(fontIt != HeAssets::textPool.end()) {
        auto& map = HeAssets::textPool[font];
        auto t = map.find(string);
       
        if(t != map.end())
            text->vao = &t->second;
    }
    
    if(text->vao == nullptr) {
        // text wasnt loaded yet (or got deleted again). Load it now!
        text->vao = &HeAssets::textPool[font][string];
        heLoadText(text, font, string);
    }
    
}


HeConstraint operator/(const HeConstraint& l, const HeConstraint& r) {

	HeConstraint result = l;
	result.vector = l.vector / r.vector;
	return result;

}

HeConstraint operator/(const HeConstraint& l, const hm::vec2f& r) {

	HeConstraint result = l;
	result.vector = l.vector / r;
	return result;

}


bool hePointInShape(const HeWindow* window, const HeShape* shape, const HeConstraint& position) {
    
    hm::vec2i pixel_point = heGetConstraint(&position, HE_CONSTRAINT_TYPE_PIXEL_SPACE, window);
    hm::vec2i pixel_posit = heGetConstraint(&shape->position, HE_CONSTRAINT_TYPE_PIXEL_SPACE, window);
    hm::vec2i pixel_size = heGetConstraint(&shape->size, HE_CONSTRAINT_TYPE_PIXEL_SPACE, window);
    
    if (pixel_posit.x > pixel_point.x ||
        pixel_posit.y > pixel_point.y ||
        pixel_posit.x + pixel_size.x < pixel_point.x ||
        pixel_posit.y + pixel_size.y < pixel_point.y)
        return false;
    
    return true;
    
}

HeConstraint heCreateWindowConstraintFromPixelSpace(const hm::vec2f& vector, const hm::vec2i& windowSize,
                                                    const HeConstraintFlag flags) {
    
    HeConstraint cs;
    cs.type = HE_CONSTRAINT_TYPE_WINDOW_SPACE;
    cs.flags = flags;
    cs.nativeWindowSize = windowSize;
    cs.vector = (vector / windowSize);
    return cs;
    
}

HeConstraint heCreatePixelConstraintFromWindowSpace(const hm::vec2f& vector, const hm::vec2i& windowSize) {
    
    HeConstraint cs;
    cs.type = HE_CONSTRAINT_TYPE_PIXEL_SPACE;
    cs.vector = vector * windowSize;
    cs.nativeWindowSize = windowSize;
    cs.flags = HE_CONSTRAINT_FLAG_KEEP_ABSOLUTE_SIZE;
    return cs;
    
}

HeConstraint heCreateConstraint(const hm::vec2f& vector, const hm::vec2i& windowSize, const HeConstraintType type, 
                                const HeConstraintFlag flags) {
    
    HeConstraint cs;
    cs.type = type;
    cs.flags = flags;
    cs.vector = vector;
    cs.nativeWindowSize = windowSize;
    return cs;
    
}

hm::vec2f heGetConstraint(const HeConstraint* constraint, const HeConstraintType targetType, const HeWindow* window) {
    
    hm::vec2f vector(0.0f);
    hm::vec2f windowSize = window->windowInfo.size;
    
    if (constraint->flags & HE_CONSTRAINT_FLAG_KEEP_ABSOLUTE_SIZE)
        windowSize = constraint->nativeWindowSize;
    
    if (constraint->flags & HE_CONSTRAINT_FLAG_KEEP_RATIO) {
        
        float nativeRatio = constraint->nativeWindowSize.x / constraint->nativeWindowSize.y;
        float newRatio = windowSize.x / windowSize.y;
        
        if (nativeRatio > newRatio)
            windowSize.y = (constraint->nativeWindowSize.y / constraint->nativeWindowSize.x) * windowSize.x;
        else
            windowSize.x = (nativeRatio * windowSize.y);
    }
    
    
    if (     targetType == HE_CONSTRAINT_TYPE_WINDOW_SPACE && constraint->type == HE_CONSTRAINT_TYPE_WINDOW_SPACE)
        vector = constraint->vector;
    else if (targetType == HE_CONSTRAINT_TYPE_PIXEL_SPACE  && constraint->type == HE_CONSTRAINT_TYPE_PIXEL_SPACE)
        vector = constraint->vector;
    else if (targetType == HE_CONSTRAINT_TYPE_PIXEL_SPACE  && constraint->type == HE_CONSTRAINT_TYPE_WINDOW_SPACE)
        vector = constraint->vector * windowSize;
    else if (targetType == HE_CONSTRAINT_TYPE_WINDOW_SPACE && constraint->type == HE_CONSTRAINT_TYPE_PIXEL_SPACE) {
        vector = constraint->vector / windowSize;
    } else {
        std::cout << "Error: Cannot convert from constraint type [" + std::to_string(constraint->type) + "] to type ["
            + std::to_string(targetType) + "]!" << std::endl;
        return hm::vec2d(0.);
    }
    
    return vector;
    
}

