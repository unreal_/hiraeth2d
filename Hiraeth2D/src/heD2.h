#pragma once
#include "heAssets.h"
#include "heShapes.h"

enum HeD2ObjectFlags {
	// if this flag is set, the object will be checked for collision in the HeD2 collision functions
	HE_D2_OBJECT_FLAG_COLLIDABLE = 0b0001,
	// if this flag is set, the objects animation is automatically increased in the render function
	HE_D2_OBJECT_FLAG_ANIMATED   = 0b0010,
	// if this flag is set, this object will be taken in account for any shadows
	HE_D2_OBJECT_FLAG_NO_SHADOWS = 0b0100
};

enum HeD2LightSourceType {
	HE_D2_LIGHT_SOURCE_TYPE_DIRECTIONAL,
	HE_D2_LIGHT_SOURCE_TYPE_POINT,
	HE_D2_LIGHT_SOURCE_TYPE_SPOT,
};

struct HeSpriteAnimationFrame {
	// the offset of this frame, in texture_space ([0:1])
	hm::vec2f offset = hm::vec2f(0);
	// the size of this frame, in texture_space ([0:1])
	hm::vec2f size = hm::vec2f(0);
};

struct HeSpriteComponent {
	// a pointer to a texture in the HeAssets::texturePool
	HeTexture* texture = nullptr;
	// the duration of all animation frames combined (in milliseconds)
	int totalAnimationTime = 0;
	// the current time of the animation in milliseconds (range [0:totalAnimationTime])
	int currentAnimationTime = 0;
	// maps the different animation frames to their time offsets (in milliseconds)
	// the first frame will have a time offset of zero, the second one (assuming we have 60fps) will have a
	// time offset of 16
	std::map<unsigned int, HeSpriteAnimationFrame> frames;
};

struct HeD2Object {
	// the sprite of this object
	HeSpriteComponent sprite;
	// the position of this object, in pixel_space
	hm::vec2f position = hm::vec2f(0);
	// the scale of this object, in pixel_space
	hm::vec2i size = hm::vec2f(0);
	// the rotation of this object, around the z-axis (into the screen)
	float rotation = 0.f;
	// the depth of this object, in layers. Objects with higher depth will be behind objects will lower depth values
	int depth = 0;
	// a list of flags this object has. Different flags have different meaning. New flags can be introduced using
	// the get_flag method in the HeD2 namespace. This will generate a new id for the given flag (or return one
	// if the flag was already used previously)
	uint32_t flags;
};

struct HeD2Camera {
	// the actual position of the camera, in pixel_space
	hm::vec2f position = hm::vec2f(0);
	// the zoom of the camera (range [0:1]). If zoom is 0, screen_space is directly linked to pixel_space,
	// meaning that 1 pixel on screen is one pixel of the room. If zoom is 1, the whole screen is filled
	// with one pixel of the room
	float zoom = 0;
	// the orthographic projection matrix of this camera, calculated every frame in the renderer
	hm::mat4 matrix;
};

struct HeD2LightSource {
	// the type of this light source
	HeD2LightSourceType type;
	// the direction of this light if this is directional
	// else the position in pixels
	hm::vec2f vector;
	// the colour of this light, with the alpha value used as intensity
	hm::colour colour;
	// data describing this light, dependant of the type
	// Directional light:
	// - no additional data
	// Point light:
	// 0. the radius of this light in pixels
	// Spot light:
	// 0. x direction of this light, normalized (-1,1)
	// 1. y direction of this light, normalized (-1,1)
	// 2. the angle of this light (radius of the light circle)
	// 3. the falloff factor of this light. The smaller the factor, the sharper the edge
	float data[4];
};

struct HeD2Level {
	// the original view size. This is the size that should be used for setting the object sizes / positions.
	// If the display size is not the view size (window resizing, smaller / bigger monitors), this view (and therefore
	// the objects) will get scaled up / down. You should assume 1920x1080 as the default monitor size
	hm::vec2i viewSize;
	// the size of this level in pixels
	hm::vec2i levelSize;
	// the camera of this level
	HeD2Camera camera;
	// a vector of lights in this level. The shader only supports a maximum number of lights, keep that mind
	std::vector<HeD2LightSource> lights;
	// the objects in this level, mapped by their type. Types can be any string, they can be used to detect certain
	// collisions, interactions...
	std::unordered_map<std::string, std::vector<HeD2Object>> objects;
	// describe what happens to the objects of this level when the display is resized. It can be one of the following:
	// HE_CONSTRAINT_FLAG_RESIZE: the scale of the objects are according to the scale of the display
	//                            if the width of the display gets doubled, the objects x-scale will get doubled, too
	//                            this can lead to distortion of the objects
	// HE_CONSTRAINT_FLAG_KEEP_RATIO: the ratio of the objects scale will remain the same, meaning that if the width
	//                                of the display gets doubled, but the height remains the same, the objects scale
	//                                wont be changed
	// HE_CONSTRAINT_FLAG_KEEP_ABSOLUTE_SIZE: the objects scale will always be one, meaning that the actual pixel size
	//                                        of the objects will not change
	HeConstraintFlag resizeFlags = HE_CONSTRAINT_FLAG_KEEP_RATIO;
	// a background texture which can be set (can be null too if no background texture is wanted). This texture
	// will be rendered relative to the room, not the view, meaning that the view of the texture is related to
	// the camera position (top left of texture is not always top left of screen)
	HeTexture* background = nullptr;
};


// returns the current frame (x_offset, y_offset, x_size, y_size) in texture space
extern HE_API hm::vec4f heGetSpriteAnimationFrame(const HeSpriteComponent* sprite);
// increases the current animation of given sprite component by given time (in milliseconds)
// if the new time is bigger than the components animations duration, the time is looped around 0 
// to restart the animation
extern HE_API void heIncreaseAnimationTime(HeSpriteComponent* sprite, const int time);
// returns true if given point is inside the bounding box of the given object
extern HE_API bool heCollidesWith(const HeD2Object* object, const hm::vec2f& point);
// creates a new directional light at given position, with given colour and given direction. The colour's alpha
// value is the intensity of the light, direction is the normalized x and y direction of the light (i.e. a direction
// of (0,1) will point directly down)
extern HE_API void heCreateDirectionalLight(HeD2LightSource* source, const hm::vec2f& position, const hm::colour& 
	colour, const hm::vec2f& direction);
// creates a new point light at given position, with given colour and given radius. Radius is the maximum distance
// an object can have to the light to still be affected by it. The light will decay over distance.
extern HE_API void heCreatePointLight(HeD2LightSource* source, const hm::vec2f& position, const hm::colour& colour,
	const float radius);
// creates a new spot light with the given parameters, as described above
extern HE_API void heCreateSpotLight(HeD2LightSource* source, const hm::vec2f& position, const hm::colour& colour,
	const hm::vec2f& direction, const float angle, const float falloff);

namespace HeD2 {
	// returns true if no object (with a hitbox) is at given position (in pixel_space)
	// object will not be checked for collision (can be set to nullptr to check for everything)
	extern HE_API bool place_free(const HeD2Object* object, const HeD2Level* level, const hm::vec2f& point);

	// returns true if object collides with any other object in the given level at given position position and
	// the objects current size
	// this will check the four corners of the objects bounding box for collision
	extern HE_API bool check_collision(const HeD2Object* object, const HeD2Level* level, const hm::vec2f& point);

	// returns an int (with one bit set) as a name for this flag. The flags "collidable" and "animated" are hardcoded
	// into the engine and will return the values set in the enum (see above)
	extern HE_API unsigned int get_flag(const std::string& name);
};


// loads a sprite component from a file. This file should be a h2s file. If this sprite is animated, the animation
// data is expected to be in the first line of the file, starting with an '@h2 '
extern HE_API void heLoadSprite(HeSpriteComponent* component, const std::string& spriteFile);
// loads a level file. This will override any data stored in level to new data. The level file must be a valid h2l
// file. If the level file does not exist, an error message is printed and level will not be changed at all
// spriteFolder is the folder all sprites are in (including the last /). This is usually "res/sprites". Subfolders
// for the sprites may be in the level file (level0/)
extern HE_API void heLoadLevel(HeD2Level* level, const std::string& levelFile, const std::string& spriteFolder);