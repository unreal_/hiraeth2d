#pragma once
#include <vector>
#include <unordered_map>

struct HeAudioBuffer {
	unsigned int id;
};

struct HeAudioSource {
	unsigned int id;
};

struct HeAudioListener {
	std::vector<HeAudioSource> sources;
};

typedef std::unordered_map<std::string, HeAudioBuffer> HeAudioBufferPool;

namespace HeAssets {
	extern HeAudioBufferPool audioPool;
};

extern void heCreateBuffer(HeAudioBuffer* buffer, const std::string& fileName);
extern void 