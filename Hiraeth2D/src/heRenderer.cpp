#include "heRenderer.h"
#include <algorithm>

HeWidgetPointer::HeWidgetPointer(const HeWidget& widget) : wdg(widget) {
	this->isPointer = false;
}

HeWidgetPointer::HeWidgetPointer(HeWidget* widget) : ptr(widget) {
	this->isPointer = true;
}


void heCreateRenderEngine(HeRenderEngine* engine, HeWindow* window) {
	
	heCreateVao(&engine->quadVao);
	heBindVao(&engine->quadVao);
    
	const float vertices = 1.f;
    
	HeVbo vbo;
	vbo.dimensions = 2;
	heCreateVbo(&vbo, { -vertices, -vertices,
                vertices, -vertices,
				-vertices,  vertices,
				-vertices,  vertices,
                vertices, -vertices,
                vertices,  vertices, });
    
	heAddVbo(&engine->quadVao, &vbo);
	heLoadShader(&engine->guiShader, "res/shaders/guiv.glsl", "res/shaders/guif.glsl");
	heLoadShader(&engine->spriteShader, "res/shaders/spritev.glsl", "res/shaders/spritef.glsl");
	heLoadShader(&engine->shadowShader, "res/shaders/shadowv.glsl", "res/shaders/shadowf.glsl");
	heLoadShader(&engine->finalShader, "res/shaders/finalv.glsl", "res/shaders/finalf.glsl");
	engine->window = window;

	engine->d2Fbo.size = window->windowInfo.size;
	engine->d2Fbo.flags = HE_FBO_FLAG_NONE;
	engine->d2Fbo.samples = 1;
	heCreateFbo(&engine->d2Fbo);
	heCreateColourAttachment(&engine->d2Fbo);

	engine->shadowFbo.size = window->windowInfo.size;
	engine->shadowFbo.flags = HE_FBO_FLAG_HDR;
	engine->shadowFbo.samples = 1;
	heCreateFbo(&engine->shadowFbo);

};

void heDestroyRenderEngine(HeRenderEngine* engine) {
	
	heDestroyShader(&engine->guiShader);
	heDestroyShader(&engine->spriteShader);
	heDestroyShader(&engine->shadowShader);
	heDestroyFbo(&engine->d2Fbo);
	heDestroyFbo(&engine->shadowFbo);
	heDestroyVao(&engine->quadVao);
    
};

void heResizeRenderEngine(HeRenderEngine* engine, const hm::vec2i& newSize) {

	heResizeFbo(&engine->d2Fbo, newSize);
	heResizeFbo(&engine->shadowFbo, newSize);

}

void hePrepareUiEngine(HeRenderEngine* engine) {
    
	heBindVao(&engine->quadVao);
    
	// update projection matrix
	// right, left, top, bottom, far, near
	const float r = engine->window->windowInfo.size.x - 1.0f, 
    l = 0.0f, 
    t = 0.0f,
    b = engine->window->windowInfo.size.y - 1.0f, 
    f = 10.0f, 
    n = -1.0f;
	
	hm::mat4 orthoMat = hm::mat4(1.0f);
	orthoMat[0][0] = 2.0f / (r - l);
	orthoMat[1][1] = 2.0f / (t - b);
	orthoMat[2][2] = 2.0f / (f - n);
	orthoMat[3][0] = -(r + l) / (r - l);
	orthoMat[3][1] = -(t + b) / (t - b);
	orthoMat[3][2] = -(f + n) / (f - n);

	heBindShader(&engine->guiShader);
	heLoadShaderUniform(&engine->guiShader, "u_projMat", orthoMat);
    
};

void hePrepareSpriteEngine(HeRenderEngine* engine, HeD2Level* level, const hm::vec2f& scale) {

	heBindVao(&engine->quadVao);

	// calculate the pixel difference for the zoom per side
	// if zoom is 0.5 and size is (100,100), cut 25 pixels on each side of the screen, so that the resulting
	// screen is (25, 25) (75, 75) 
	const hm::vec2f zoom = (hm::vec2f(engine->window->windowInfo.size) * level->camera.zoom);
	const hm::vec2f camOffset = level->camera.position * scale;

	level->camera.matrix = hm::createOrthographic(hm::vec2f(engine->window->windowInfo.size) - zoom, camOffset, 10.f);

	heBindShader(&engine->spriteShader);
	heLoadShaderUniform(&engine->spriteShader, "u_projMat", level->camera.matrix);

}

void heFinishEngine(HeRenderEngine* engine) {
    
	heUnbindShader();
	heUnbindVao(&engine->quadVao);
    
};

void heRenderWidget(HeRenderEngine* engine, HeWidget* widget) {

	if (widget->type == HE_WIDGET_TYPE_SHAPE)
		heRenderShape(engine, &widget->shape, &widget->input, widget->depth);
	else if (widget->type == HE_WIDGET_TYPE_TEXT)
		heRenderText(engine, &widget->text, &widget->input, widget->depth);

};

void heRenderShape(HeRenderEngine* engine, const HeShape* shape, const HeColourInput* input, const float depth) {
    
	HeShaderProgram* shad = &engine->guiShader;
	hm::mat4 transMat(1.f);
	hm::vec2f size = heGetConstraint(&shape->size, HE_CONSTRAINT_TYPE_PIXEL_SPACE, engine->window);
	hm::vec2f center = heGetConstraint(&shape->position, HE_CONSTRAINT_TYPE_PIXEL_SPACE, engine->window) + size / 2;
	transMat = hm::translate(transMat, hm::vec3f(center.x, center.y, depth));
	transMat = hm::rotate(transMat, shape->rotation, hm::vec3f(0, 0, 1.0f));
	transMat = hm::scale(transMat, hm::vec3f(size.x / 2.f, size.y / 2.f, 1.f));
    
	heLoadShaderUniform(shad, "u_transMat", transMat);
	heLoadShaderUniform(shad, "u_shapeType", shape->type);
	heLoadColourInputToShader(shad, input, 0);
	heBindVao(&engine->quadVao);
	heRenderVao(&engine->quadVao);
    
};

void heRenderText(HeRenderEngine* engine, HeText* text, const HeColourInput* input, const float depth) {
    
	HeShaderProgram* shad = &engine->guiShader;
	hm::mat4 transMat(1.f);
	hm::vec2f size = heGetConstraint(&text->scale, HE_CONSTRAINT_TYPE_PIXEL_SPACE, engine->window);
	hm::vec2f center = heGetConstraint(&text->position, HE_CONSTRAINT_TYPE_PIXEL_SPACE, engine->window);
	transMat = hm::translate(transMat, hm::vec3f(center.x, center.y, (float) depth));
	transMat = hm::rotate(transMat, text->rotation, hm::vec3f(0, 0, 1.f));
	transMat = hm::scale(transMat, hm::vec3f(size.x / 2.f, size.y / 2.f, 1.f));
	
	hm::vec2f realSize = heGetConstraint(&text->scale, HE_CONSTRAINT_TYPE_PIXEL_SPACE, engine->window);
	float textSize = realSize.y / text->font->fontSizeInPixels;
    
	float boldness = 0.55f + (0.12f * max(0.f, 1.f - textSize));
	float softness = 0.01f + (0.22f * max(0.f, 1.f - textSize));
    
	// if the text wasnt loaded yet, load it now
	if (text->vao == nullptr)
		heGetText(text, text->font, text->string);

	heBindTexture(text->font->texture, heGetShaderSamplerLocation(shad, "t_fontAtlas", 0));
	heLoadShaderUniform(shad, "u_shapeType", HE_SHAPE_TYPE_TEXT);
	heLoadShaderUniform(shad, "u_shapeData", hm::vec2f(boldness, softness));
	heLoadShaderUniform(shad, "u_transMat", transMat);
	heLoadColourInputToShader(shad, input, 0);
	heBindVao(text->vao);
	heRenderVao(text->vao);
    
}

void heRenderTexture(HeRenderEngine* engine, HeTexture* texture, const hm::vec2i& position, const hm::vec2f& scale) {

	HeShaderProgram* shad = &engine->spriteShader;

	hm::vec2f scl = hm::vec2f((float) texture->width, (float) texture->height) * scale;
	hm::vec2f pos = position * scale;
	hm::vec2f center = pos + scl / 2.f;

	hm::mat4 transMat(1.0f);
	transMat = hm::translate(transMat, hm::vec3f(center.x, center.y, 1.0f));
	transMat = hm::scale(transMat, hm::vec3f((float) scl.x / 2.f, (float) scl.y / 2.f, 1.0f));

	heLoadShaderUniform(shad, "u_transMat", transMat);
	heLoadShaderUniform(shad, "u_animFrame", hm::vec4f(0, 0, 1, 1)); // display whole texture (sprite shader)
	heBindTexture(texture, heGetShaderSamplerLocation(shad, "u_spriteAtlas", 0));
	heRenderVao(&engine->quadVao);

}

void heRenderTexture(HeRenderEngine* engine, const unsigned int texture) {

	heBindTexture(texture, 0);
	heRenderVao(&engine->quadVao);

}

void heRenderD2Object(HeRenderEngine* engine, HeD2Object* object, const hm::vec2f& scale) {

	if (object->flags & HE_D2_OBJECT_FLAG_ANIMATED)
		// if this object is an animated object, increase the animation time
		heIncreaseAnimationTime(&object->sprite, int(engine->window->frameTime * 1000));

	HeShaderProgram* shad = &engine->spriteShader;
	hm::vec2f pos = object->position * scale;
	hm::vec2f scl = hm::vec2f(object->size) * scale;

	hm::vec2f center = pos + scl / 2.f;
	hm::mat4 transMat(1.0f);
	transMat = hm::translate(transMat, hm::vec3f(center.x, center.y, (float) object->depth));
	transMat = hm::rotate(transMat, object->rotation, hm::vec3f(0, 0, 1.0f));
	transMat = hm::scale(transMat, hm::vec3f(scl.x / 2.f, scl.y / 2.f, 1.0f));

	// load shader uniforms
	heLoadShaderUniform(shad, "u_transMat", transMat);
	heLoadShaderUniform(shad, "u_animFrame", heGetSpriteAnimationFrame(&object->sprite));
	heLoadShaderUniform(shad, "u_castShadows", !(object->flags & HE_D2_OBJECT_FLAG_NO_SHADOWS));
	heBindTexture(object->sprite.texture, heGetShaderSamplerLocation(shad, "u_spriteAtlas", 0));
	heRenderVao(&engine->quadVao);

}

void heRenderUiQueue(HeRenderEngine* engine) {
    
	if (engine->d2Fbo.size != engine->window->windowInfo.size)
		heResizeRenderEngine(engine, engine->window->windowInfo.size);

	hePrepareUiEngine(engine);
    
	for (HeWidgetPointer& widgets : engine->queue) {
		/*switch (w.type) {
		case 0:
			// widget_ptr
			heRenderWidget(engine, items.wdgptr);
			break;

		case 1:
			heRenderWidget(engine, &items.wdg);
			break;

		case 2:
			heRenderD2Object(engine, items.objptr);
			break;
		}*/

		HeWidget* ptr = (widgets.isPointer) ? widgets.ptr : &widgets.wdg;
		heRenderWidget(engine, ptr);
	}

	engine->queue.clear();
	heFinishEngine(engine);
    
}

void heRenderD2Level(HeRenderEngine* engine, HeD2Level* level) {

	if (level->objects.size() == 0)
		return;

	if (engine->d2Fbo.size != engine->window->windowInfo.size)
		heResizeRenderEngine(engine, engine->window->windowInfo.size);

	// get window scale
	hm::vec2f scaleFactor(1.0f); // default behaviour: keep absolute size
	if (level->resizeFlags == HE_CONSTRAINT_FLAG_KEEP_RATIO) {
		float nativeRatio = (float)level->viewSize.x / level->viewSize.y;
		float newRatio = (float)engine->window->windowInfo.size.x / engine->window->windowInfo.size.y;

		scaleFactor = hm::vec2f((float)engine->window->windowInfo.size.x / level->viewSize.x,
			(float)engine->window->windowInfo.size.y / level->viewSize.y);

		if (nativeRatio > newRatio)
			scaleFactor.y = scaleFactor.x;
		else
			scaleFactor.x = scaleFactor.y;
	}

	// prepare engine and render background
	hePrepareSpriteEngine(engine, level, scaleFactor);
	if (level->background != nullptr)
		heRenderTexture(engine, level->background, hm::vec2i(0), scaleFactor);

	// render all objects into fbo
	{
		heBindFbo(&engine->d2Fbo);
		heClearFrame(hm::colour(0, 0, 0, 0), 0);

		for (auto& types : level->objects)
			for (HeD2Object& objs : types.second)
				heRenderD2Object(engine, &objs, scaleFactor);
	}

	// create shadow map from that fbo
	{
		heBindFbo(&engine->shadowFbo);
		heBindShader(&engine->shadowShader);
		heClearFrame(hm::colour(0, 0, 0, 0), 0);
		heLoadShaderUniform(&engine->shadowShader, "u_clip", hm::vec4f(0, 0, (float)engine->window->windowInfo.size.x,
			(float)engine->window->windowInfo.size.y));
		heLoadShaderUniform(&engine->shadowShader, "u_fboSize", engine->d2Fbo.size);
		heBlendMode(1);
		
		for (HeD2LightSource& sources : level->lights) {
			// calculate vector for this light.
			// if this is directional, simply take direction
			// if this is point / spot, calculate position on screen by substracting camera
			
			//hm::vec2f vector = sources.type == HE_D2_LIGHT_SOURCE_TYPE_DIRECTIONAL ? 
				//hm::vec2f(sources.data[0], sources.data[1]) : sources.vector - level->camera.position;

			hm::vec2f vector = sources.vector;
			if (sources.type != HE_D2_LIGHT_SOURCE_TYPE_DIRECTIONAL) {
				vector = vector * scaleFactor;
				hm::vec4f screenCoords = level->camera.matrix * hm::vec4f(vector.x, vector.y, 1.0f, 1.0f);
				vector = (hm::vec2f(screenCoords) * 0.5 + 0.5) * engine->d2Fbo.size;
			}

			hm::vec4f data;
			data.x = vector.x;
			data.y = vector.y;
			data.z = sources.data[0] * scaleFactor.y;

			heLoadShaderUniform(&engine->shadowShader, "u_tint", sources.colour);
			heLoadShaderUniform(&engine->shadowShader, "u_lightData", data);
			heLoadShaderUniform(&engine->shadowShader, "u_lightType", sources.type);
			heRenderTexture(engine, engine->d2Fbo.colourTextures[1]);
		}
	}

	// render final result
	{
		heBlendMode(0);
		heUnbindFbo(engine->window->windowInfo.size);
		heBindShader(&engine->finalShader);
		heBindTexture(engine->d2Fbo.colourTextures[0], heGetShaderSamplerLocation(&engine->finalShader, "u_diffuseTexture", 0));
		heBindTexture(engine->shadowFbo.colourTextures[0], heGetShaderSamplerLocation(&engine->finalShader, "u_shadowTexture", 1));
		heRenderVao(&engine->quadVao);
	}
	
	heFinishEngine(engine);

}


void heUpdateWidgetInput(HeWindow* window, HeWidget* widget) {
    
	HeWidgetStatus status = HE_WIDGET_STATUS_NONE;
	HeMouseInfo* mouseInfo = &window->mouseInfo;
    
	if (hePointInShape(window, &widget->shape, mouseInfo->mousePosition)) {
		status = HE_WIDGET_STATUS_HOVERED;
		if (mouseInfo->leftButtonDown)
			status = status | HE_WIDGET_STATUS_LEFT_BUTTON_DOWN;
        
		if (mouseInfo->rightButtonDown)
			status = status | HE_WIDGET_STATUS_RIGHT_BUTTON_DOWN;
	}
	
	widget->status = status;
    
}

bool heUiPushWidget(HeRenderEngine* engine, HeWidget* widget) {
    
	engine->queue.emplace_back(widget);
	if(widget->type == HE_WIDGET_TYPE_SHAPE)
		heUpdateWidgetInput(engine->window, widget);
	return widget->status & HE_WIDGET_STATUS_LEFT_BUTTON_DOWN;
    
}

bool heUiPushWidget(HeRenderEngine* engine, HeWidget& widget) {

	engine->queue.emplace_back(HeWidgetPointer(widget));
	if (widget.type == HE_WIDGET_TYPE_SHAPE)
		heUpdateWidgetInput(engine->window, &widget);
	return widget.status & HE_WIDGET_STATUS_LEFT_BUTTON_DOWN;

}

bool heUiPushShape(HeRenderEngine* engine, const HeShape& shape, const HeColourInput& input) {
    
	HeWidget widget(HE_WIDGET_TYPE_SHAPE);
	widget.input = input;
	widget.shape = shape;
	engine->queue.emplace_back(widget);
	heUpdateWidgetInput(engine->window, &widget);
	return widget.status & HE_WIDGET_STATUS_LEFT_BUTTON_DOWN;
    
}

void heUiPushText(HeRenderEngine* engine, HeFont* font, const std::string& text, const hm::colour& colour) {
    
    HeColourInput input(HE_COLOUR_INPUT_TYPE_SOLID_COLOUR);
    input.solid.colour = colour;
    
    HeWidget widget(HE_WIDGET_TYPE_TEXT);
    widget.input = input;
	widget.text.font = font;
	heGetText(&widget.text, font, text);
    engine->queue.emplace_back(widget);
    
}

bool heUiPushButton(HeRenderEngine* engine, const std::string& text, const HeConstraint& position) {

	size_t hash = std::hash<std::string>{}(text); 
	HeWidget shapeWidget = engine->defaultButtonWidget;
	shapeWidget.shape.position = position;
	if (shapeWidget.input.type == HE_COLOUR_INPUT_TYPE_TRANSITION)
		shapeWidget.input.transition.currentTime = engine->widgetTransitionTimes[hash];
	
	shapeWidget.depth = 2;
	heUiPushWidget(engine, shapeWidget);

	HeWidget textWidget(HE_WIDGET_TYPE_TEXT);
	textWidget.input = HeColourInput(HE_COLOUR_INPUT_TYPE_SOLID_COLOUR);
	textWidget.input.solid.colour = engine->defaultButtonColour;

	textWidget.text.scale = heCreateConstraint(
		hm::vec2f(shapeWidget.shape.size.vector.y * (16.f / 9.f) * 0.7f, shapeWidget.shape.size.vector.y * 0.7f),
		engine->window->windowInfo.size, shapeWidget.shape.size.type, HE_CONSTRAINT_FLAG_KEEP_RATIO);
	textWidget.text.position = position;
	textWidget.text.font = engine->defaultFont;
	textWidget.text.string = text;

	textWidget.depth = 1;
	heUiPushWidget(engine, textWidget);

	heUpdateWidgetInput(engine->window, &shapeWidget);

	if (shapeWidget.status & HE_WIDGET_STATUS_HOVERED)
		engine->widgetTransitionTimes[hash] = hm::clamp(
			engine->widgetTransitionTimes[hash] + engine->window->frameTime, 0.0, 1.0);
	else
		engine->widgetTransitionTimes[hash] = hm::clamp(
			engine->widgetTransitionTimes[hash] - engine->window->frameTime, 0.0, 1.0);


	return shapeWidget.status & HE_WIDGET_STATUS_LEFT_BUTTON_DOWN;

}