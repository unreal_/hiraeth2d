#pragma once
#include "hm/hm.hpp"
#include "heGlLayer.h"
#include "heShapes.h"
#include <map>
#include <vector>

#pragma warning(disable : 26812) // disable enum unscoped warning
#pragma warning(disable : 26495) // disable union variable unitialized warning

enum HeColourInputType {
	HE_COLOUR_INPUT_TYPE_NONE,
	HE_COLOUR_INPUT_TYPE_SOLID_COLOUR,
	HE_COLOUR_INPUT_TYPE_GRADIENT,
	HE_COLOUR_INPUT_TYPE_TEXTURE,
	HE_COLOUR_INPUT_TYPE_TRANSITION
};

enum HeWidgetStatus {
	HE_WIDGET_STATUS_NONE              = 0,
	HE_WIDGET_STATUS_HOVERED           = 0b0001,
	HE_WIDGET_STATUS_LEFT_BUTTON_DOWN  = 0b0010,
	HE_WIDGET_STATUS_RIGHT_BUTTON_DOWN = 0b0100
};

enum HeWidgetType {
	HE_WIDGET_TYPE_SHAPE,
	HE_WIDGET_TYPE_TEXT
};

struct HeColourInput {	
	HeColourInputType type = HE_COLOUR_INPUT_TYPE_NONE;
    
	union {
		struct {
			hm::colour colour;
		} solid;
        
		struct {
			std::map<uint32_t, hm::colour> steps;
		} gradient;
        
		struct {
			HeTexture* texture = nullptr;
		} texture;
        
		struct {
			// for now we only support 2 steps: one at time 0 and one at time 1
			std::vector<HeColourInput> steps;
			// the current time of the transition, between 0 and 1
			double currentTime;
		} transition; 
	};
    
	HE_API HeColourInput() {};
	HE_API HeColourInput(const HeColourInputType type);
	HE_API HeColourInput(const HeColourInput& input);
	HE_API ~HeColourInput();
	HE_API HeColourInput& operator=(const HeColourInput& input);
    
};

struct HeWidget {
	// only set (useful content) if the type of this widget is shape
	HeShape shape;
	// only set (useful content) if the type of this widget is text
	HeText  text;
    
	HeWidgetType  type;
	HeColourInput input;	
	// this contains one or more HeWidgetStatus flags depending on its current status. When a widget is rendered ('heDoWidget')
	// the input system will automatically set all flags that are currently true for this widget
	HeWidgetStatus status = HE_WIDGET_STATUS_NONE;
    
	// the depth of this widget. The higher the depth (positive numbers), the "farther" it is from screen,
	// meaning that widgets with a smaller depth will be on top of higher depth widgets
	float depth = 1;

	HE_API HeWidget() {};
	HE_API HeWidget(const HeWidgetType type);
	HE_API HeWidget(const HeWidget& widget);
	HE_API ~HeWidget();
};


inline HeWidgetStatus operator| (HeWidgetStatus a, HeWidgetStatus b) { return (HeWidgetStatus)((int)a | (int)b); }; \
inline HeWidgetStatus operator& (HeWidgetStatus a, HeWidgetStatus b) { return (HeWidgetStatus)((int)a & (int)b); };


// loads a colour input to the given shader. inputIndex is the index of the colour input to load (0-1). This is called for
// every shape rendered. If input is a transition, it will load the two affected steps to the shader (with indices 0, 1),
// if input is anything else, inputIndex should be 0
extern HE_API void heLoadColourInputToShader(HeShaderProgram* program, const HeColourInput* input, const unsigned int inputIndex);
// adds a new gradient step to the given colour input. Input's type must be set to HE_COLOUR_INPUT_TYPE_GRADIENT for this
// position should be in range of [0:1] for both x and y, where (0,0) is top left and (1,1) is down right
extern HE_API void heAddGradientStep(HeColourInput* input, const hm::vec2d& position, const hm::colour& colour);