#include "heGui.h"

HeColourInput::HeColourInput(const HeColourInputType type) {
	this->type = type;
	switch (this->type) {
	case HE_COLOUR_INPUT_TYPE_GRADIENT:
		new (&gradient.steps) std::map<uint32_t, hm::colour>();
		break;

	case HE_COLOUR_INPUT_TYPE_TRANSITION:
		new (&transition.steps) std::vector<HeColourInput>();
		break;
	}
}

HeColourInput::HeColourInput(const HeColourInput& input) {
	*this = this->operator=(input);
}

HeColourInput::~HeColourInput() {
	switch (this->type) {
	case HE_COLOUR_INPUT_TYPE_GRADIENT:
		gradient.steps.~map();
		break;
	
	case HE_COLOUR_INPUT_TYPE_TRANSITION:
		transition.steps.~vector();
		break;
	}
}

HeColourInput& HeColourInput::operator=(const HeColourInput& input) {
	if (this != &input) {

		this->type = input.type;

		switch (this->type) {
		case HE_COLOUR_INPUT_TYPE_GRADIENT:
			new (&gradient.steps) std::map<uint32_t, hm::colour>();
			break;

		case HE_COLOUR_INPUT_TYPE_TRANSITION:
			new (&transition.steps) std::vector<HeColourInput>();
			break;
		}

		switch (this->type) {
		case HE_COLOUR_INPUT_TYPE_GRADIENT:
			this->gradient = input.gradient;
			break;

		case HE_COLOUR_INPUT_TYPE_SOLID_COLOUR:
			this->solid = input.solid;
			break;

		case HE_COLOUR_INPUT_TYPE_TEXTURE:
			this->texture = input.texture;
			break;

		case HE_COLOUR_INPUT_TYPE_TRANSITION:
			this->transition = input.transition;
			break;
		}
	}

	return *this;
}


HeWidget::HeWidget(const HeWidgetType type) {
	
	this->type = type;

}

HeWidget::HeWidget(const HeWidget& widget) {

	this->type = widget.type;
	this->input = widget.input;
	this->status = widget.status;
	this->depth = widget.depth;

	switch (this->type) {
	case HE_WIDGET_TYPE_SHAPE:
		this->shape = widget.shape;
		break;
	
	case HE_WIDGET_TYPE_TEXT:
		this->text = widget.text;
		break;
	}

}

HeWidget::~HeWidget() {

	switch (this->type) {
	case HE_WIDGET_TYPE_TEXT:
		break;
	}

}


void heLoadColourInputToShader(HeShaderProgram* program, const HeColourInput* input, const unsigned int inputIndex) {

	heLoadShaderUniform(program, "u_transitionTime", (float)-1);

	if (input->type == HE_COLOUR_INPUT_TYPE_TRANSITION) {
		heLoadColourInputToShader(program, &input->transition.steps[0], 0);
		heLoadColourInputToShader(program, &input->transition.steps[1], 1);
		heLoadShaderUniform(program, "u_transitionTime", (float) input->transition.currentTime);	
	} else if (input->type == HE_COLOUR_INPUT_TYPE_GRADIENT) {
		heLoadShaderUniform(program, "u_inputs[" + std::to_string(inputIndex) + "].type", 2);

		unsigned int index = 0;
		for (const auto& all : input->gradient.steps) {
			heLoadShaderUniform(program, "u_inputs[" + std::to_string(inputIndex) + "].colours[" + std::to_string(index) + "]", all.second);
			heLoadShaderUniform(program, "u_inputs[" + std::to_string(inputIndex) + "].gradientPositions[" + std::to_string(index) + "]", hm::decodeVec2<float>(all.first) / 1000.0f);
			++index;
		}

		for (int i = index; i < 4; ++i) {
			heLoadShaderUniform(program, "u_inputs[" + std::to_string(inputIndex) + "].colours[" + std::to_string(i) + "]", hm::vec4f(-1, -1, -1, -1));
			heLoadShaderUniform(program, "u_inputs[" + std::to_string(inputIndex) + "].gradientPositions[" + std::to_string(i) + "]", hm::vec2f(-1));
		}

	} else if (input->type == HE_COLOUR_INPUT_TYPE_SOLID_COLOUR) {
		heLoadShaderUniform(program, "u_inputs[" + std::to_string(inputIndex) + "].type", 1);
		heLoadShaderUniform(program, "u_inputs[" + std::to_string(inputIndex) + "].colours[0]", input->solid.colour); 
	} else if (input->type == HE_COLOUR_INPUT_TYPE_TEXTURE) {
		heLoadShaderUniform(program, "u_inputs[" + std::to_string(inputIndex) + "].type", 0);
		heBindTexture(input->texture.texture, heGetShaderSamplerLocation(program, "u_inputs[" + std::to_string(inputIndex) + "]._texture", inputIndex + 1));
	}

};

void heAddGradientStep(HeColourInput* input, const hm::vec2d& position, const hm::colour& colour) {
	
	input->gradient.steps[hm::encodeVec2(hm::vec2i(position * 1000.))] = colour;

};