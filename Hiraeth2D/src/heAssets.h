#pragma once
#include "heGlLayer.h"
#include <unordered_map>

struct HeFontCharacter {
	float width;
	float height;
	float xoffset;
	float yoffset;
	float xadvance;
	float texx;
	float texy;
	float texwidth;
	float texheight;
};

struct HeFont {
	// the texture atlas of this font, requested from the texturePool in HeAssets
	HeTexture* texture;
    
	// padding between the characters
	int padding[4];
	// the atlas size in pixels
	unsigned int textureWidth, textureHeight;
	// the size in pixels. This is usually the size of the exclamation mark (in pixels). Used for normalizing character sizes
	unsigned int fontSizeInPixels;
	// desired padding, should always be 8
	static const int DESIRED_PADDING = 8;
    
	// maps an ascii code to a font character 
	std::map<unsigned int, HeFontCharacter> characters;
    
	// for every time this font is requested from the asset pool, this reference count goes up.
	// If this font is destroyed with heDestroyFont, referenceCount will go down.
	// Only if the referenceCount is 0, the font (the font atlas) will actually be deleted
	unsigned int referenceCount = 0;
};


// maps a font to its file the font was loaded from
typedef std::unordered_map<std::string, HeFont> HeFontPool;
// maps a texture to its file the texture was loaded from
typedef std::unordered_map<std::string, HeTexture> HeTexturePool;

namespace HeAssets {
	extern HeFontPool fontPool;
	extern HeTexturePool texturePool;
};

// loads a font from given file. If that file does not exist, font is not changed. The texture is was specified in the font
// file must be in the res folder
extern HE_API void heLoadFont(HeFont* font, const std::string& fileName);
// destroys the given font if its referenceCount is currently 1. This deletes the font atlas
extern HE_API void heDestroyFont(HeFont* font);

// gets the font with given name from the fontPool (HeAssets). If the font does not exist yet, it is loaded from the
// hard drive with given file name
extern HE_API HeFont* heGetFont(const std::string& fileName);
// gets the texture with given name from the texturePool (HeAssets). If the texture does not exist yet, it is loaded from
// the hard drive with given file name
extern HE_API HeTexture* heGetTexture(const std::string& fileName);