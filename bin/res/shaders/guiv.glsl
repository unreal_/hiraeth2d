#version 140

in vec2 in_position;
in vec2 in_uv;

out vec2 pass_uv;
flat out int pass_shapeType;

uniform mat4 u_transMat;
uniform mat4 u_projMat;

// shape types:
// 0 = rectangle,
// 1 = ellipse,
// 2 = triangle,
// 3 = text
uniform int u_shapeType;

void main(void) {
	gl_Position = u_projMat * u_transMat * vec4(in_position, 0.0, 1.0);
	if(u_shapeType == 3)
		pass_uv = in_uv;
	else {
		pass_uv = in_position * 0.5 + 0.5;
		pass_uv.y = 1.0 - pass_uv.y;
	}
	
	pass_shapeType = u_shapeType;
}
