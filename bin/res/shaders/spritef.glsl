#version 140

in vec2 pass_uv;

out vec4 out_colour;
out vec4 out_shadows;

uniform sampler2D u_spriteAtlas;
uniform bool u_castShadows;

void main(void) {
	vec4 raw_normal = vec4(0, 0, 1, 1);
	
	if(raw_normal.a == 0.0)
		// no light here
		out_colour = vec4(0.0);
	else {
		raw_normal -= 0.5;
		vec3 lightDirection = vec3(1, 1, 1);
		vec3 lightColour = vec3(0.9, 0.9, 0.9);
		
		float lightWeight = dot(normalize(raw_normal.xyz), normalize(-lightDirection));
		lightWeight = max(lightWeight, 0.0);
		lightWeight = 1.0;
		//vec4(lightColour * lightWeight, 1.0) * 
		out_colour = texture2D(u_spriteAtlas, pass_uv);
	
		if(u_castShadows)
			out_shadows = vec4(out_colour.a);
		else
			out_shadows = vec4(0);
	}
}