#version 440

in vec2 pass_uv;
flat in int pass_shapeType;

out vec4 out_colour;

const int GRADIENT_STEP_COUNT = 4;

struct ColourInput {
	int type; // 0 = texture, 1 = solid colour, 2 = gradient
	vec4 colours[GRADIENT_STEP_COUNT]; // index 0 is solid colour if type == 1, each index is a step of the gradient if type == 2
	vec2 gradientPositions[GRADIENT_STEP_COUNT]; // the positions of the gradient steps if type == 2
	sampler2D _texture;
};

uniform ColourInput[2] u_inputs; // maximum of 4 steps per transition
uniform float u_transitionTime;
uniform vec2 u_shapeData;
uniform sampler2D t_fontAtlas;


vec4 calculateGradientColour(vec4 colours[GRADIENT_STEP_COUNT], vec2 positions[GRADIENT_STEP_COUNT]) {
	vec2 gradPos = pass_uv ;
	
	float totalDistance = 0.f;
	float weights[GRADIENT_STEP_COUNT];

	for(int i = 0; i < GRADIENT_STEP_COUNT; ++i) {
		if(colours[i] != vec4(-1)) {
			vec2 realPos = vec2(positions[i].x, 1. - positions[i].y);
			float dist = 2. - (abs((realPos.x - gradPos.x) * (realPos.x - gradPos.x)) + abs((realPos.y - gradPos.y) * (realPos.y - gradPos.y)));
			totalDistance += dist;
			weights[i] = dist;
		}
	}

	vec4 finalColour = vec4(0);

	for(int i = 0; i < GRADIENT_STEP_COUNT; ++i) {
		if(colours[i] != vec4(-1)) {
			float percent = clamp(weights[i] / totalDistance, 0., 1.);
			finalColour += colours[i] * percent;
		}
	}
	
	return finalColour;
} 

vec4 calculateTransitionStepColour(int index) {
	vec4 colour = vec4(0);

	switch(u_inputs[index].type) {
		case 0:
			colour = texture2D(u_inputs[index]._texture, pass_uv);
			break;
		
		case 1:
			colour = u_inputs[index].colours[0];
			break;
	
		case 2:
			colour = calculateGradientColour(u_inputs[index].colours, u_inputs[index].gradientPositions);
			break;
		
		default:
			colour = vec4(1, 0, 0, 1);
	}	
	
	return colour;
}

float calculateEllipseAlpha() {

	float dist = ((pass_uv.x - 0.5) * (pass_uv.x - 0.5)) / 0.25 + ((pass_uv.y - 0.5) * (pass_uv.y - 0.5)) / 0.25;
	return smoothstep(1.01, 0.99, dist);

}

float calculateTextAlpha() {

	float dist = texture(t_fontAtlas, pass_uv).a;
    return smoothstep(1.0 - u_shapeData.x, (1.0 - u_shapeData.x) + u_shapeData.y, dist);
	//return 1.0;

}

float calculateShapeAlpha() {

	switch(pass_shapeType) {
		case 0:
		return 1.0;
		
		case 1:
		return calculateEllipseAlpha();
		
		case 2:
		return 1.0;
		
		case 3:
		return calculateTextAlpha();
	}
	
	return 0;
	
}

void main(void) {

	vec4 fragColour;
	if(u_transitionTime == -1.0)
		fragColour = calculateTransitionStepColour(0);
	else {
		vec4 step0 = calculateTransitionStepColour(0);
		vec4 step1 = calculateTransitionStepColour(1);
		fragColour = (1.0 - u_transitionTime) * step0 + u_transitionTime * step1;
	}
	
	float alpha = calculateShapeAlpha();
	
	out_colour = vec4(fragColour.rgb, fragColour.a * alpha);
	//out_colour = vec4(alpha, alpha, alpha, 1.0);
	//out_colour = vec4(pass_uv, 0.0, 1.0);

}