#version 330 core

in vec2 in_position;

out vec2 pass_uv;

uniform mat4 u_projMat;
uniform mat4 u_transMat;
uniform vec4 u_animFrame;

void main(void) {
	gl_Position = u_projMat * u_transMat * vec4(in_position, 0.0, 1.0);
	vec2 uv_pos = in_position * 0.5 + 0.5;
	uv_pos.y = uv_pos.y;
	//        frame offset   +  with / height  * normalized uv position
	pass_uv = u_animFrame.xy + (u_animFrame.zw * uv_pos);
	pass_uv.y = 1.0 - pass_uv.y;
}