#version 330 core

in vec2 pass_uv;
in vec2 pass_position;

out vec4 out_colour;

uniform sampler2D u_texture; // the d2 texture with all shadow casters
uniform vec4 u_clip;         // a box on screen in which to cast shadows
uniform vec4 u_tint;         // a tint for the shadows
uniform vec2 u_fboSize;      // the size of the d2 fbo
uniform vec4 u_lightData;    // information about the light
uniform int u_lightType;     // the type of this light. Possible types are:
							 // 0 = directional
							 // 1 = point			

const float minLight = 0.1;

float getAttenuation(float radius, float _distance) {
	/*float denom = (_distance - radius) / radius + 1;
	float att = 1.0 / (denom * denom);
	att = (att - cutoff) / (1.0 / cutoff);
	return max(att, 0.0);
	return 1;*/

	
	float a = 0.0;
	float b = 1.0 / (radius * radius * minLight);
	return 1.0 / (1.0 + a * _distance + b * _distance * _distance);
}

vec3 getLightDirection() {
	vec2 fragPos = gl_FragCoord.xy;

	if(u_lightType == 0)
		return vec3(-u_lightData.x, u_lightData.y, 1.0); // this is the light direction, inverse it
		
	if(u_lightType == 1) {
		vec2 light = u_lightData.xy - fragPos; // direction from object to light		
		float dist = dot(light, light);
		if(dist > u_lightData.z)
			// point is outside of light range
			return vec3(0);
		
		light = normalize(light);
		return vec3(light.x, light.y, getAttenuation(u_lightData.z, dist));
	}
	
	return vec3(0);
}
				
void main(void) {
	vec3 lightDir = getLightDirection();
	
	if(lightDir.x == 0.0 && lightDir.y == 0.0)
		discard;

	if(texture(u_texture, pass_uv).a > 0.0 &&
		gl_FragCoord.x >= u_clip.x && gl_FragCoord.x <= u_clip.z &&
		gl_FragCoord.y >= u_clip.y && gl_FragCoord.y <= u_clip.w) {
		
		// fragment is inside the clip and on an object
		out_colour = vec4(0); // ambient light?

		vec2 check_uv = pass_uv;
		float brightness = (0.2126 * out_colour.r + 0.7152 * out_colour.g + 0.0722 * out_colour.b); // ambient light

		vec2 texelSize = lightDir.xy / u_fboSize;

		for(int i = 0; i < 32; i++) {
			check_uv += texelSize;
			out_colour.a += ((1.0 - texture(u_texture, check_uv)).a / (13 + pow(2, i / 3.0) + brightness * 100));
		}
		
		out_colour.rgb = u_tint.rgb * u_tint.a * lightDir.z;
		out_colour.a *= u_tint.a; //  * lightDir.z
	} else
		discard;
}