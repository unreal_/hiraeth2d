#version 140

in vec2 pass_uv;

out vec4 out_colour;

uniform sampler2D u_diffuseTexture;
uniform sampler2D u_shadowTexture;

void main(void) {
	vec4 light = texture(u_shadowTexture, pass_uv);
	light.a = 1.0;
	light = max(light, 0.2);

	vec4 diffuse = texture(u_diffuseTexture, pass_uv);
	vec4 hdr = diffuse * light;
	out_colour.rgb = hdr.rgb / (hdr.rgb + vec3(1.0)); // convert hdr
	out_colour.a = diffuse.a;
	//out_colour.a = 1.0;
	//out_colour.rgb = vec3(diffuse.a);

	//out_colour = texture(u_shadowTexture, pass_uv);
	//out_colour.a = 1.0;
}